from generator import Image_Generator, Basis_Generator
from pytorch_discriminator import Discriminator
import h5py
import warnings
import argparse
import os
import csv
import time

#from pytket.backends.ibm import AerStateBackend, AerBackend, AerUnitaryBackend, IBMQBackend
#from pytket.backends.projectq import ProjectQBackend
from qiskit import *
from qiskit import Aer

from qiskit.aqua.components.optimizers import ADAM

import torch.optim as optim

import autograd.numpy as np
from math import floor

import torch
import matplotlib.pyplot as plt

from dual_qgan import QGAN


def generate_backend(backend_name, readout_noise_only=False):
    """
    Generate an instance of a backend which can either be the state vector simulator, the qasm simulator with various noise models 
    """
    # Set quantum instance to run the quantum generator
    if backend_name == 'statevector_simulator':
        # The state-vector simulator
        # quantum_instance = QuantumInstance(
        #     backend=Aer.get_backend('statevector_simulator'))

        backend=Aer.get_backend('statevector_simulator')
    elif backend_name == 'qasm_simulator':
        # The qasm simulator mimicing a perfect quantum computer
        # quantum_instance = QuantumInstance(
        #     backend=Aer.get_backend('qasm_simulator'))
        backend=Aer.get_backend('qasm_simulator')
    elif readout_noise_only:
        # We mimic a noisy backend but we only keep readout errors
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        # After retrieving the full noise model, we serialize it into a dictionary and delete any noise that is not readout noise
        noise_model_dict = noise_model.to_dict()
        # This dictionary should have two entries with key "errors", "x90_gates", the second one is typically empty and I have no idea what it is good for
        if ('x90_gates' in noise_model_dict) and noise_model_dict['x90_gates']:
            warnings.warn(
                "Entry for x_90_gates in noise model dictionary not empty!")
        # We now examine the list of dictionaries specifying the errors and only keep those which are related to readout error
        tmp = list()
        for entry in noise_model_dict['errors']:
            if entry['type'] == 'roerror':
                # We found an entry corresponding to a readout error, keep it
                tmp.append(entry)
        noise_model_dict['errors'] = tmp
        noise_model = NoiseModel.from_dict(noise_model_dict)
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)
    else:
        # We mimic a noisy backend
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)

    return backend




def load_training_data(title = 'C:/Users/suchang/cernbox/Documents/data/classified_data_4pixels2.h5'):
    n_clusters = 4
    hf = h5py.File(title, 'r')
    xd = hf.get('image_data')
    labels = hf.get('labels')

    X_train = xd[:20000, :]
    labels = labels[:20000]
    hf.close()

    train_data = [[] for _ in range(n_clusters)]

    for x, l in zip(X_train, labels):
        train_data[l].append(x)

    image_dist = np.array([len(x) for x in train_data])
    mean_images = np.array([np.mean(x, axis = 0) for x in train_data])

    tuple_data = sorted(zip(image_dist, mean_images), key = lambda x : x[0])
    image_dist = np.array([x[0] for x in tuple_data])
    mean_images = np.array([x[1] for x in tuple_data])

    image_dist = image_dist/np.sum(image_dist)


    return X_train


def setup_qgan(X_train, num_epochs, lr1, lr2, lr_disc, path):

    #Discriminator architecture
    nn_architecture = [{"input_dim": 4, "output_dim": 256, "activation": "leaky_relu"},
                        {"input_dim": 256, "output_dim": 128, "activation": "leaky_relu"},
                                {"input_dim": 128, "output_dim": 1, "activation": "sigmoid"}
                            ]




    depth1 = 2
    depth2 =  6

    # Number of qubits of PQCs (n2 > n1)
    n1 = 4
    n2 = 4

    n = 2

    #Number of epochs
    num_epochs = 5000

    # Parameterized quantum circuits
    PQC1 = Basis_Generator(n1, n, depth1, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n1*(depth1 + 1))])
    PQC2 = Image_Generator(n1, n2, n, depth2, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n2*(depth2 + 1))])

    PQC1.set_optimizer(ADAM(maxiter=1, tol=1e-6, lr=lr1, beta_1=0.7,
                                beta_2=0.99, noise_factor=1e-6,
                                eps=1e-6, amsgrad=True))

    PQC2.set_optimizer(ADAM(maxiter=1, tol=1e-6, lr=lr2, beta_1=0.7,
                                beta_2=0.99, noise_factor=1e-6,
                                eps=1e-6, amsgrad=True))

    netD = Discriminator(nn_architecture, lr = 1e-4)
    netD.set_optimizer(optim.Adam(netD._netD.parameters(), lr=lr_disc, amsgrad = True))

    PQC1.set_discriminator(netD)
    PQC2.set_discriminator(netD)



    qgan = QGAN(X_train,n1, n2, batch_size = 2000)

    qgan.set_PQC1(PQC1)
    qgan.set_PQC2(PQC2)

    qgan.set_discriminator(netD)

    return qgan

def plot_results(X_train, num_epochs, d_loss, g_loss1, g_loss2, rel_entr, weights, images, figname="", path = ""):
    "Visualize final results"

    t_steps = np.arange(num_epochs)

    plt.figure(figsize=(11-3/4, 8-1/4))

    plt.subplot(221)
    t_steps = np.arange(num_epochs)
    plt.title("Progress in the loss function")
    
    plt.plot(t_steps, d_loss,
             label="D_loss", linewidth=2)
    plt.plot(t_steps, g_loss1, label="G_loss1", linewidth=2)
    plt.plot(t_steps, g_loss2, label="G_loss2", linewidth=2)
    plt.grid()
    plt.legend(loc='best')
    plt.xlabel('time steps')
    plt.ylabel('loss')

    # Plot progress w.r.t relative entropy
    plt.subplot(222)
    plt.title("Relative Entropy ")
    plt.plot(t_steps, rel_entr, lw=2)
    plt.grid()
    plt.xlabel('time steps')
    plt.ylabel('relative entropy')
    plt.yscale("log")

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(223)
    plt.title("Mean image")
    x = np.linspace(0,3, 4)
    plt.bar(x,   np.sum(np.array([w*i for w,i in zip(weights, images)]), axis = 0), width=0.8, label='Simulated')
    plt.plot(x, np.mean(X_train, axis = 0), '--o', label='Target', linewidth=2,
             markersize=12, color='tab:orange')

    plt.xlabel('Calorimeter Depth')
    plt.ylabel('Normalized Energy')
    plt.legend(loc='best')

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(224)
    plt.title("Images")
    x = np.linspace(0,3, 4)
    plt.plot(x, np.transpose(images))

    plt.xlabel('Calorimeter Depth')
    plt.ylabel('Normalized Energy')


    # Some beautyfication and save final results
    plt.tight_layout()
    if figname:
        plt.savefig(path + figname + '.pdf')
    # plt.show()
    plt.close("all")



if __name__ == "__main__":

    path = ""

    parser = argparse.ArgumentParser(
        description="Run qGAN experiments on simulator")
    parser.add_argument('--backend_name', type=str, default='statevector_simulator',
                        help='the name of the backend we are using/emulating')
    parser.add_argument('--num_shots_pqc1', type=int, default=-1,
                        help='the number of shots for PQC1 we are going to use in case we emulate a device, a negative value indicates that we take the same number as the batch size (default)')
    parser.add_argument('--num_shots_pqc2', type=int, default=1024,
                        help='the number of shots for PQC2 we are going to use in case we emulate a device (default 1024)')                    
    parser.add_argument('--num_epochs', type=int, default=1000,
                        help='the number of epochs used for training (default 30)')
    parser.add_argument('--shift_rule', type=int, default=1,
                        help='flag indicating if parameter shift rule is used to compute the gradients or not (default is True)')
    parser.add_argument('--readout_noise_only', type=int, default=0,
                        help='specify if only readout_noise is kept (1) or the full noise model is used (0, default)')
    parser.add_argument('--learning_rate_pqc1', type=float, default=1E-4,
                        help='learning rate for training (default 1E-4)')
    parser.add_argument('--learning_rate_pqc2', type=float, default=1E-3,
                        help='learning rate for training (default 1E-3)')                    
    parser.add_argument('--learning_rate_disc', type=float, default=1E-4,
                        help='learning rate for training (default 1E-4)')   

    args = parser.parse_args()

    # Display input
    args.shift_rule = args.shift_rule == 1
    print("Training qGAN using")
    print(" - num_epochs:        ", args.num_epochs)
    print(" - backend:           ", args.backend_name)
    print(" - shift_rule:        ", args.shift_rule)
    print(" - learning_rate_pqc1: ", args.learning_rate_pqc1)
    print(" - learning_rate_pqc2: ", args.learning_rate_pqc2)
    print(" - learning_rate_disc ", args.learning_rate_disc)

    if args.backend_name != 'statevector_simulator':
        print(" - num_shots_pqc1:         ", args.num_shots_pqc1)
        print(" - num_shots_pqc2:         ", args.num_shots_pqc2)
        print(" - only bitflip:      ", args.readout_noise_only)

    
    
    # Transform the input into a boolean variable
    ro_noise_only = False
    if args.readout_noise_only == 1:
        ro_noise_only = True

    statevector = False
    if args.backend_name == "statevector_simulator":
        statevector = True
    


    # set file name
    count = 1

    if args.backend_name == 'statevector_simulator':
        to_add = 'statevector_simulator_shift_rule_' + str(args.shift_rule) + '_epochs' + \
            str(args.num_epochs) + '_run' 
        
    else:
        if args.backend_name == 'qasm_simulator':
            to_add = "_" + args.backend_name + '_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots_pqc1) + '_' + str(args.num_shots_pqc2) + '_run'
        elif ro_noise_only:
            to_add =  "_" + args.backend_name + '_readout_noise_only_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots_pqc1) + '_' + str(args.num_shots_pqc2) + '_run'
        else:
            to_add = "_" + args.backend_name + '_full_noise_shift_rule_' + str(args.shift_rule) + '_epochs' + \
                str(args.num_epochs) + '_shots' + str(args.num_shots_pqc1) + '_' + str(args.num_shots_pqc2) + '_run'

    # Get a backend
    quantum_instance = generate_backend(args.backend_name, ro_noise_only)


    # '/data/suchang/data/classified_data_4pixels2.h5'
    X_train = load_training_data()
    qgan = setup_qgan(X_train, args.num_epochs, args.learning_rate_pqc1, args.learning_rate_pqc2, args.learning_rate_disc, path)

    d_loss, g_loss1, g_loss2, rel_entr, weights, images =  qgan.train(quantum_instance,
                                                                          args.num_epochs, SV = statevector, shots1 = args.num_shots_pqc1, 
                                                                          shots2 = args.num_shots_pqc2, PS = args.shift_rule,
                                                                          path = path, to_add = to_add)

    


    figname = 'dualPQC' + to_add + str(count)

    while os.path.isfile(path + figname + '.pdf'):
            count += 1
            figname = 'dualPQC' + to_add + str(count)

    plot_results(X_train, args.num_epochs, d_loss, g_loss1, g_loss2, rel_entr, weights, images, figname=figname, path = path)
