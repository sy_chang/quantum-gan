import torch

import random
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils

from torch.autograd import Variable

import numpy as np

#Initialize weights
def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Linear') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.5)


class Discriminator_Net(nn.Module) :
    def __init__(self, nn_architecture):
        super(Discriminator_Net, self).__init__()

        # Initialize the pytorch discriminator

        modules = []


        for d in nn_architecture :
            modules.append(nn.Linear(d['input_dim'], d['output_dim']))

            if d['activation'] == 'sigmoid':
                modules.append(nn.Sigmoid())
            elif d['activation'] == 'leaky_relu':
                modules.append(nn.LeakyReLU(0.2))
            elif d['activation'] == 'relu':
                modules.append(nn.ReLU())
            else :
                raise Exception('Non-supported activation function')

        self._main  = nn.Sequential(*modules)

    def forward(self, input_data):
        return self._main(input_data)


class Discriminator:
    def __init__(self, nn_architecture, lr = 0.01):
        self._netD = Discriminator_Net(nn_architecture)
        self._netD.apply(weights_init)

        # Use adam optimizer
        self._optimizer = optim.Adam(self._netD.parameters(), lr=lr, betas=(0.9, 0.999))

        # Define criterion for discriminator loss
        self._criterion = nn.BCELoss()

    # Get labels from image data
    def get_labels(self, images, detach = False):
        if isinstance(images, torch.Tensor):
            pass
        else:
            images = torch.tensor(images, dtype=torch.float32)
            images = Variable(images)


        if detach :
            predicted = self._netD(images.detach())
            predicted = predicted.detach().numpy()

            return np.reshape(predicted, (len(predicted), ))

        else :
            return self._netD(images)

    # Set optimizer
    def set_optimizer(self, optimizer):
        self._optimizer = optimizer


    # Train discriminator
    def train(self, images, labels):
        if isinstance(images, torch.Tensor):
            pass
        else:
            images = torch.tensor(images, dtype=torch.float32)
            images = Variable(images)

        if isinstance(labels, torch.Tensor):
            pass
        else :
            labels = torch.tensor(labels, dtype=torch.float32)


        self._netD.zero_grad()

        output = self._netD(images).view(-1)

        errD = self._criterion(output, labels)

        # Calculate the gradients for this batch
        errD.backward()


        # Update D
        self._optimizer.step()

        return output.detach().numpy(), errD.detach().numpy()
