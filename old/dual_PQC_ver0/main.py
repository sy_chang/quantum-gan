from qGAN_ver0 import QGAN
from generator import Image_Generator, Basis_Generator
from pytorch_discriminator import Discriminator
import h5py

from pytket.backends.ibm import AerStateBackend, AerBackend, AerUnitaryBackend, IBMQBackend
from pytket.backends.projectq import ProjectQBackend

from qiskit.aqua.components.optimizers import ADAM

import numpy as np
from math import floor

from get_data import getData

# import data
X_train = getData(64)



depth1 = 3
depth2 = 3

# Number of qubits of PQCs (n2 > n1)
n1 = 3
n2 = 5

#Number of epochs
num_epochs = 5

# Parameterized quantum circuits
PQC1 = Basis_Generator(n1, depth1)
PQC2 = Image_Generator(n1, n2, depth2)


#Discriminator architecture
nn_architecture = [{"input_dim": 8, "output_dim": 24, "activation": "leaky_relu"},
                    {"input_dim": 24, "output_dim": 12, "activation": "leaky_relu"},
                    {"input_dim": 12, "output_dim": 6, "activation": "leaky_relu"},
                            {"input_dim": 6, "output_dim": 1, "activation": "sigmoid"}
                        ]


netD = Discriminator(nn_architecture)


qgan = QGAN(X_train, n1, n2, batch_size = 1000)

qgan.set_PQC1(PQC1)
qgan.set_PQC2(PQC2)
qgan.set_discriminator(netD)

quantum_instance = AerBackend()


qgan.train(quantum_instance, num_epochs, shots = 1024)
