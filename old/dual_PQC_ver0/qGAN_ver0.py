from generator_ver0 import Basis_Generator, Image_Generator
from pytorch_discriminator_ver0 import Discriminator

import numpy as np

#Quantum GAN
class QGAN:

    def __init__(self, data, n1, n2, batch_size = 1):
        self._data = data
        self._n1 = n1
        self._n2 = n2


        self._g_loss1 = []
        self._g_loss2 = []
        self._d_loss = []

        self._batch_size = batch_size


    # Set PQC1
    def set_PQC1(self, g_circuit = None):
        if g_circuit is None :
            self._PQC1 = Basis_Generator(self._n1,  3)
        else :
            self._PQC1 = g_circuit

        return self._PQC1._parameters


    # Set PQC2
    def set_PQC2(self, g_circuit = None):
        if g_circuit is None :
            self._PQC2 = Image_Generator(self._n2,  3)
        else :
            self._PQC2 = g_circuit

        return self._PQC2._parameters

    # Set Disciminator
    def set_discriminator(self, discriminator = None):
        self._discriminator = discriminator


    def PQC1(self):
        return self._PQC1

    def PQC2(self):
        return self._PQC2

    def discriminator(self):
        return self._discriminator

    # Train QGAN
    def train(self, quantum_instance, num_epochs, shots = 1000):
        for epoch in range(num_epochs):
            num_batches = len(self._data)//self._batch_size

            real_labels = np.ones(self._batch_size)
            fake_labels = np.ones(self._batch_size)

            self._PQC1.set_image_generator(self._PQC2)
            self._PQC2.set_basis_generator(self._PQC1)

            d_loss_mean = []
            g_loss1_mean = []
            g_loss2_mean = []

            for n in range(num_batches):

                self._PQC1.set_image_generator(self._PQC2)
                self._PQC2.set_basis_generator(self._PQC1)

                real_image = self._data[n*self._batch_size : (n+1)*self._batch_size]
                generated_image = self._PQC2.get_images(quantum_instance, self._PQC1._parameters, self._PQC2._parameters, num_images = self._batch_size, shots = 1024)


                # 1. Train Discriminator
                _, d_loss_real = self._discriminator.train(real_image, real_labels)
                _, d_loss_fake = self._discriminator.train(generated_image, fake_labels)

                d_loss_mean.append((d_loss_real + d_loss_fake)/2.)

                self._PQC1.set_discriminator(self._discriminator)
                self._PQC2.set_discriminator(self._discriminator)

                g_loss1_mean.append(0.0)
                g_loss2_mean.append(0.0)

                # As we train the discriminator twice, once for real images and once for fake images,
                # we also train generator twice
                for _ in range(2) :
                    # 2. Train PQC1
                    g_loss1_mean[-1] += self._PQC1.train(quantum_instance, real_labels, shots)/2.

                    # 3. Train PQC2
                    g_loss2_mean[-1] += self._PQC2.train(quantum_instance, real_labels, shots)/2.

            self._d_loss.append(np.mean(d_loss_mean))
            self._g_loss1.append(np.mean(g_loss1_mean))
            self._g_loss2.append(np.mean(g_loss2_mean))
