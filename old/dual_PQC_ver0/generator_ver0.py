import pytket
from pytket import Circuit
from pytket.utils import counts_from_shot_table


import numpy as np

from qiskit.aqua.components.optimizers import ADAM


# Quantum generator

class Generator:
    def __init__(self, num_qubits,depth, init_parameters = None):
        self._num_qubits = num_qubits # Number of qubits in PQC
        self._depth = depth # depth of PQC

        # Initialize parameters
        if init_parameters is None :
            self._parameters = np.array([np.random.normal() for _ in range((self._depth+1)*self._num_qubits)])

        # Initialize optimizer
        self._optimizer = ADAM()

    # Construct PQC from given paremters
    def init_layers(self, circuit, parameters):
        # Use RY variational form
        def layer(c, params):
            N = len(c.qubits)
            for i in range(N):
                c.Ry(params[i], i)

            for i in range(N-1):
                c.CX(i, i+1)


        for i in range(self._num_qubits):
            circuit.Ry(parameters[0,i], i)

        for d in range(self._depth):
            layer(circuit,parameters[d,:])
        return circuit

    # Set optimizer for generator
    def set_optimizer(self, optimizer = None):
        if optimizer is None:
            self._optimizer = ADAM()
        else :
            self._optimizer = optimizer



    def set_lr(self, lr):
        self._optimizer._lr = lr

    # Use BCE loss function
    def loss_function(self, actual, predicted):
        sum_score = 0.0
        for i in range(len(actual)):
            sum_score += (actual[i] * np.log(max(1e-15, predicted[i])) + \
                         (1.0 - actual[i]) * np.log(max(1e-15, 1.0 - predicted[i])))
        mean_sum_score = 1.0 / len(actual) * sum_score

        return -mean_sum_score


    # Set discriminator to calculate loss
    def set_discriminator(self, discriminator):
        self._discriminator = discriminator

    # Get output from PQC
    def get_output(self, quantum_instance, params, num_outputs = 1024,
                    get_counts = True, input_state = None,seed = 1 ):

        # Initialize circuit
        circuit = Circuit(self._num_qubits)

        # In case we have an input state, set the input state using X gates
        # (For PQC2)
        if input_state is not None :
            for j, b in enumerate(input_state):
                if b == 1:
                    circuit.X(j)

        # Construct circuit according to the given parameter
        circuit = self.init_layers(circuit, np.reshape(params, (self._depth+1, self._num_qubits)))

        # Compile circuit
        quantum_instance.compile_circuit(circuit)

        # Using state_vector simulator
        if quantum_instance is "state_vector" :

            result = quantum_instance.process_circuit(circuit)
            result = quantum_instance.get_state(result)
            probs = np.multiply(result, np.conj(result))
            probs = probs.real

            # In case we have to return the exact occurrence of the outputs
            if get_counts :
                counts = np.rint(probs*num_outputs)


                if np.sum(counts) > num_outputs:
                    decimals = sorted(list(enumerate((probs*num_outputs)%1)), key=lambda x: x[1])
                    for i in range(np.sum(counts) - num_outputs) :
                        counts[decimals[i][0]] -= 1

                elif np.sum(counts) <  num_outputs :
                    decimals = sorted(list(enumerate((probs*num_outputs)%1)),
                                        key=lambda x: x[1], reverse = True)
                    for i in range(np.sum(counts) - num_outputs) :
                        counts[decimals[i][0]] += 1


                output = {tuple(map(int, np.binary_repr(s, width = self._num_qubits))) : counts[s] \
                            for s in range(len(counts))}
            else :
                # In case we have to return probability distribution
                output = probs

        else :
            # In case we have to return the exact occurrence of the outputs
            circuit.measure_all()
            shot_handle = quantum_instance.process_circuit(circuit, n_shots = num_outputs, seed = seed)
            shots = quantum_instance.get_shots(shot_handle)
            result = counts_from_shot_table(shots)


            if get_counts :
                output = result
            else :
                # In case we have to return probability distribution
                output = np.zeros(2**self._num_qubits)

                for nbin, count in result.items() :
                    s = sum([nbin[i]*2**(self._num_qubits - i - 1) for i in range(self._num_qubits)])
                    output[s] = count

                output = output/num_outputs

        return output


    def get_objective_function(self, quantum_instance, labels, discriminator):
        return

    #Train generator
    def train(self,quantum_instance, labels,shots = 1024, seed = 1):
        self._shots = shots
        self._num_images = len(labels)

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        objective = self.get_objective_function(quantum_instance, labels, self._discriminator)

        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            initial_point=self._parameters
            )

        return loss

# PQC1
class Basis_Generator(Generator):
    def __init__(self, n, depth):
        super().__init__(n, depth)

    def set_image_generator(self, pqc2):
        self._image_generator = pqc2


    def get_basis(self, quantum_instance, params, num_outputs = 1024, seed = 1):
        return self.get_output(quantum_instance,params, num_outputs, get_counts = True, seed = seed)

    def get_objective_function(self, quantum_instance, labels, discriminator):
        def objective_function(params):

            generated_images = self._image_generator.get_images(quantum_instance,  params, self._image_generator._parameters,
                                                num_images = self._num_images, shots=self._shots,
                                                seed = self._seed)

            prediction_generated = discriminator.get_labels(generated_images, detach=True)

            return self.loss_function(labels, prediction_generated)

        return objective_function




#PQC2
class Image_Generator(Generator):
    def __init__(self, n1, n2, depth):
        if n1 > n2 :
            tmp = n2
            n2 = n1
            n1 = tmp

        super().__init__(n2, depth) # n2 is the number of qubits in PQC2

        self._n1 = n1


    def set_basis_generator(self, pqc1):
        self._basis_generator = pqc1


    # Get images from computational basis
    def get_images(self, quantum_instance, pqc1_params, pqc2_params, num_images = 1024, shots = 1024, seed = 1):
        input_basis = self._basis_generator.get_basis(quantum_instance, pqc1_params,
                                                        num_outputs = num_images, seed = seed)
        output_images = np.empty((0, 2**self._n1))


        for basis, val in input_basis.items() :

            image = self.get_output(quantum_instance, pqc2_params, num_outputs = shots, get_counts = False,
                                    input_state= basis, seed = seed)

            image = image[0:2**self._n1]/np.sum(image[0:2**self._n1])

            output_images = np.vstack((output_images, np.tile(image,(val, 1))))
        np.random.shuffle(output_images)

        return output_images

    #Objective function for training
    def get_objective_function(self, quantum_instance, labels, discriminator):

        def objective_function(params):

            generated_images = self.get_images(quantum_instance, self._basis_generator._parameters, params,
                                                num_images = self._num_images, shots=self._shots,
                                                seed = self._seed)
            prediction_generated = discriminator.get_labels(generated_images, detach=True)
            prediction_generated.flatten()
            return self.loss_function(labels, prediction_generated)

        return objective_function
