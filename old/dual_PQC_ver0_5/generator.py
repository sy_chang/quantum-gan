import pytket
from pytket import Circuit
from pytket.utils import counts_from_shot_table


from qiskit.aqua.components.optimizers import ADAM

import autograd
import autograd.numpy as np


from qiskit.aqua import aqua_globals
from qiskit.aqua.components.optimizers.optimizer import Optimizer

import time


class Generator:
    def __init__(self, num_qubits, n,  depth, init_parameters = None):
        self._depth = depth

        self._num_qubits = num_qubits
        self._n = n

        if init_parameters is None :
            self._parameters = np.array([np.random.normal() for _ in range((self._depth+1)*self._num_qubits)])
#             self._parameters = np.zeros((self._depth+1)*self._num_qubits)

        else :
            self._parameters = init_parameters


        self._optimizer = ADAM()

    def init_layers(self, circuit, parameters):
        def layer(c, params):

            for i in range(self._num_qubits-1):
                c.CZ(i, i+1)

            for i in range(self._num_qubits):
                c.Ry(params[i], i)

        for i in range(self._num_qubits):
            circuit.Ry(parameters[0,i], i)

        for d in range(1, self._depth+1):
            layer(circuit,parameters[d,:])

        return circuit

    def set_optimizer(self, optimizer = None):
        if optimizer is None:
            self._optimizer = ADAM()
        else :
            self._optimizer = optimizer



    def set_lr(self, lr):
        self._optimizer._lr = lr

    def loss_function(self, x, weights):
        try:
            # pylint: disable=no-member
            loss = (-1) * np.dot(np.log(x).transpose(), weights)
        except Exception:  # pylint: disable=broad-except
            loss = (-1) * np.dot(np.log(x), weights)
        return loss.flatten()

    def set_discriminator(self, discriminator):
        self._discriminator = discriminator

    def get_output(self, quantum_instance, params, qitype = "state_vector", num_outputs = 1024, input_state = None,seed = 1 ):
        circuit = Circuit(self._num_qubits)

        if input_state is not None :
            if input_state is "uniform" :
                for i in range(self._num_qubits):
                    circuit.H(i)

            else :
                for j, b in enumerate(input_state):
                    if b == '1':
                        circuit.X(j)

        circuit = self.init_layers(circuit, np.reshape(params, (self._depth+1, self._num_qubits)))

        quantum_instance.compile_circuit(circuit)

        if qitype is "state_vector" :

            result = quantum_instance.process_circuit(circuit)
            result = quantum_instance.get_state(result)
            probs = np.multiply(result, np.conj(result))
            probs = probs.real

            output = probs

        else :
            circuit.measure_all()
            shot_handle = quantum_instance.process_circuit(circuit, n_shots = num_outputs, seed = seed)
            shots = quantum_instance.get_shots(shot_handle)
            result = counts_from_shot_table(shots)

            output = np.zeros(2**self._num_qubits)

            for nbin, count in result.items() :
                s = sum([nbin[i]*2**(self._num_qubits - i - 1) for i in range(self._num_qubits)])
                output[s] = count

            output = output/num_outputs

        return output


    def get_objective_function(self, quantum_instance, discriminator, qitype = "state_vector"):
        return

    def train(self,quantum_instance, qitype = "state_vector",shots = 1024, seed = 1):
        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        objective = self.get_objective_function(quantum_instance, self._discriminator,qitype)

        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            initial_point=self._parameters
            )

        return loss


class Basis_Generator(Generator):
    def __init__(self, num_qubits,n, depth, init_parameters = None):
        super().__init__(num_qubits, n, depth, init_parameters = init_parameters)


    def get_weights(self, quantum_instance, params, qitype = "state_vector", num_outputs = 1024, seed = 1):
        output = self.get_output(quantum_instance,params, qitype, num_outputs, input_state = "uniform", seed = seed)
        return output[:2**self._n]/np.sum(output[:2**self._n])

    def set_images(self, images) :
        self._images = images


    def get_objective_function(self, quantum_instance, discriminator, qitype = "state_vector"):
        def objective_function(params):

            output  = self.get_weights(quantum_instance, params, qitype, num_outputs = self._shots, seed = self._seed)

            prediction_generated = discriminator.get_labels(self._images, detach=True)
            return self.loss_function(prediction_generated, output)

        return objective_function




class Image_Generator(Generator):
    def __init__(self, n1, n2,n, depth, init_parameters = None):
        if n1 > n2 :
            tmp = n2
            n2 = n1
            n1 = tmp

        super().__init__(n2,n, depth, init_parameters = init_parameters)

        self._n1 = n1


    def set_weights(self, weights):
        self._weights = weights

    def get_images(self, quantum_instance, params, qitype = "state_vector", shots = 1024, seed = 1):
        input_basis = [np.binary_repr(s, width = self._num_qubits) for s in range(2**self._n)]

        output_images = []


        for b in input_basis :

            image = self.get_output(quantum_instance, params, qitype, num_outputs = shots, input_state= b, seed = seed)

            image = image[0:2**self._n]/np.sum(image[0:2**self._n])

            output_images.append(image)

        return np.array(output_images)




    def single_objective_function(self, quantum_instance, discriminator, basis, qitype = "state_vector") :
        def objective_function(params):
            generated_image = self.get_output(quantum_instance, params, qitype, num_outputs = self._shots,
                                    input_state= basis, seed = self._seed)

            generated_image = generated_image[0:2**self._n]/np.sum(generated_image[0:2**self._n])

            return discriminator.get_labels(generated_image, detach=True)
        return objective_function

    def get_gradient_function(self, quantum_instance, discriminator, qitype = "state_vector"):

        def get_loss_function(weights):
            def loss(x) :
                return self.loss_function(x, weights)
            return loss

        def g_fnct(params) :

            input_basis = [np.binary_repr(s, width = self._num_qubits) for s in range(2**self._n)]
            derivatives = []
            predicted_labels = []
            for b in input_basis :

                objective_function = self.single_objective_function(quantum_instance,self._discriminator, b, qitype)

                grad = Optimizer.wrap_function(Optimizer.gradient_num_diff,
                                                                    (objective_function, self._optimizer._eps))
                derivatives.append(grad(params))

                output = self.get_output(quantum_instance, params, qitype, num_outputs = self._shots, input_state= b, seed = self._seed)

                prediction = self._discriminator.get_labels(output[0:2**self._n]/np.sum(output[0:2**self._n]), detach = True)
                predicted_labels.append(prediction)

            derivatives = np.array(derivatives)
            loss_func = get_loss_function(self._weights)
            g = autograd.grad(loss_func)
            grad = g(np.array(predicted_labels))

            return np.sum(np.array([x*y for x,y in zip(grad, derivatives)]), axis = 0)
        return g_fnct

    def global_objective_function(self, quantum_instance, discriminator, qitype = "state_vector"):
        def objective_function(params):

            generated_image = self.get_images(quantum_instance, params, qitype, shots = self._shots, seed = self._seed)
            prediction_generated = discriminator.get_labels(generated_image, detach=True)
            prediction_generated.flatten()
            return self.loss_function(prediction_generated, self._weights)

        return objective_function

    def train(self,quantum_instance, qitype = "state_vector",shots = 1024, seed = 1):
        losses = []

        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")
        grad_funct = self.get_gradient_function(quantum_instance,self._discriminator, qitype)

        objective = self.global_objective_function(quantum_instance, self._discriminator, qitype)

        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            gradient_function = grad_funct,
            initial_point=self._parameters
            )

        losses.append(loss)

        return np.mean(np.array(losses))
