from generator import Image_Generator, Basis_Generator
from pytorch_discriminator import Discriminator
import h5py

#from pytket.backends.ibm import AerStateBackend, AerBackend, AerUnitaryBackend, IBMQBackend
#from pytket.backends.projectq import ProjectQBackend
from qiskit import *
from qiskit import Aer

from qiskit.aqua.components.optimizers import ADAM

import torch.optim as optim

import autograd.numpy as np
from math import floor

import torch
import matplotlib.pyplot as plt

from dual_qgan import QGAN

n_clusters = 4

#Load data
hf = h5py.File('/data/classified_data_4pixels2.h5', 'r')
xd = hf.get('image_data')
labels = hf.get('labels')

X_train = xd[:20000, :]
labels = labels[:20000]
hf.close()

train_data = [[] for _ in range(n_clusters)]

for x, l in zip(X_train, labels):
    train_data[l].append(x)

image_dist = np.array([len(x) for x in train_data])
mean_images = np.array([np.mean(x, axis = 0) for x in train_data])

tuple_data = sorted(zip(image_dist, mean_images), key = lambda x : x[0])
image_dist = np.array([x[0] for x in tuple_data])
mean_images = np.array([x[1] for x in tuple_data])

image_dist = image_dist/np.sum(image_dist)


#Discriminator architecture
nn_architecture = [{"input_dim": 4, "output_dim": 1024, "activation": "leaky_relu"},
                   {"input_dim": 1024, "output_dim": 512, "activation": "leaky_relu"},
                    {"input_dim": 512, "output_dim": 256, "activation": "leaky_relu"},
                            {"input_dim": 256, "output_dim": 1, "activation": "sigmoid"}
                        ]
#nn_architecture = [{"input_dim": 4, "output_dim": 512, "activation": "leaky_relu"},
#                    {"input_dim": 512, "output_dim": 256, "activation": "leaky_relu"},
#                            {"input_dim": 256, "output_dim": 1, "activation": "sigmoid"}
#                        ]




depth1 = 1
depth2 =  6

# Number of qubits of PQCs (n2 > n1)
n1 = 4
n2 = 4

n = 2

#Number of epochs
num_epochs = 5000

# Parameterized quantum circuits
PQC1 = Basis_Generator(n1, n, depth1, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n1*(depth1 + 1))])
PQC2 = Image_Generator(n1, n2, n, depth2, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n2*(depth2 + 1))])

PQC1.set_optimizer(ADAM(maxiter=1, tol=1e-6, lr=1e-4, beta_1=0.7,
                               beta_2=0.99, noise_factor=1e-6,
                               eps=1e-6, amsgrad=True))

PQC2.set_optimizer(ADAM(maxiter=1, tol=1e-6, lr=1e-2, beta_1=0.7,
                               beta_2=0.99, noise_factor=1e-6,
                               eps=1e-6, amsgrad=True))

netD = Discriminator(nn_architecture, lr = 1e-4)
netD.set_optimizer(optim.Adam(netD._netD.parameters(), lr=1e-4, amsgrad = True))

PQC1.set_discriminator(netD)
PQC2.set_discriminator(netD)


quantum_instance = Aer.get_backend('statevector_simulator')
#quantum_instance = AerStateBackend()

qgan = QGAN(X_train,image_dist, n1, n2, batch_size = 2000)

qgan.set_PQC1(PQC1)
qgan.set_PQC2(PQC2)

qgan.set_discriminator(netD)

qgan.train(quantum_instance,  num_epochs, qitype = "state_vector",shots = 2000, path = "test2/")
