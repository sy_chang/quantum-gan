from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets

import h5py
from math import floor


def getData(n_clusters) :
    nevt = 10000  # number of events used for (training and testing)
    d=h5py.File("/data/suchang/data/Electron2D_data.h5",'r')

    xd = d.get('ECAL')
    print(xd.shape)

    nx = xd.shape[2]
    ny = xd.shape[3]

    X=np.array(xd[:nevt,:,:])

    # N = 8
    # N2 = 25 // N
    #
    # #Reshape image into a normalized vector of size 64
    # x_sum = []
    # for x in X:
    #     tmp = np.zeros((N,N))
    #     for i in range(N):
    #         for j in range(N):
    #             tmp[i,j] = np.mean(x[0][N2*i:N2*(i+1), N2*j:N2*(j+1)])
    #
    #     tmp = np.reshape(tmp, (N**2, ))
    #     if np.sum(tmp) > 0.0 :
    #         tmp = tmp/np.sum(tmp)
    #
    #     x_sum.append(tmp)


    x_sum = []
    for x in X:
        tmp = np.sum(x[0], axis = 0)
        x_sum.append(np.array([np.sum(tmp[6*i:6*(i+1)])for i in range(floor(len(tmp)/6))]))

        if np.sum(tmp) != 0 :
            x_sum[-1] /= np.sum(x_sum[-1])

    X_train = np.array(x_sum)

    #KMeans
    km = KMeans(n_clusters=n_clusters)
    km.fit(X_train)
    km.predict(X_train)
    labels = km.labels_

    train_data = [[] for _ in range(n_clusters)]

    for x, l in zip(X_train, km.labels_):
        train_data[l].append(x)

    final_images = np.empty((0, 4))
    for x in train_data:
        final_images = np.vstack((final_images, np.tile(np.mean(np.array(x), axis = 0),(len(x), 1))))

    np.random.shuffle(final_images)

    return final_images
