from generator import Basis_Generator, Image_Generator
from pytorch_discriminator import Discriminator

import numpy as np
import time
class QGAN:

    def __init__(self, data, n1, n2, batch_size = 1):
        self._data = data
        self._n1 = n1
        self._n2 = n2


        self._g_loss1 = []
        self._g_loss2 = []
        self._d_loss = []

        self._batch_size = batch_size



    def set_PQC1(self, g_circuit = None):
        if g_circuit is None :
            self._PQC1 = Basis_Generator(self._n1,  3)
        else :
            self._PQC1 = g_circuit

        return self._PQC1._parameters


    def set_PQC2(self, g_circuit = None):
        if g_circuit is None :
            self._PQC2 = Image_Generator(self._n2,  3)
        else :
            self._PQC2 = g_circuit

        return self._PQC2._parameters


    def set_discriminator(self, discriminator = None):
        self._discriminator = discriminator


    def PQC1(self):
        return self._PQC1

    def PQC2(self):
        return self._PQC2

    def discriminator(self):
        return self._discriminator


    def train(self, quantum_instance, num_epochs, qitype = "state_vector", shots = 1000):
        for epoch in range(num_epochs):
            print("Epoch", epoch)
            start = time.time()

            num_batches = len(self._data)//self._batch_size

            real_labels = np.ones(self._batch_size)
            fake_labels = np.ones(self._batch_size)

            self._PQC1.set_image_generator(self._PQC2)
            self._PQC2.set_basis_generator(self._PQC1)

            d_loss_mean = []
            g_loss1_mean = []
            g_loss2_mean = []

            for n in range(num_batches):

                self._PQC1.set_image_generator(self._PQC2)
                self._PQC2.set_basis_generator(self._PQC1)

                real_image = self._data[n*self._batch_size : (n+1)*self._batch_size]
                generated_image = self._PQC2.get_images(quantum_instance, self._PQC1._parameters, self._PQC2._parameters,
                                                        qitype, num_images = self._batch_size, shots = 1024)

                # 1. Train Discriminator
                _, d_loss_real = self._discriminator.train(real_image, real_labels)
                #print(d_output)

                _, d_loss_fake = self._discriminator.train(generated_image, fake_labels)

                d_loss_mean.append((d_loss_real + d_loss_fake)/2.)

                self._PQC1.set_discriminator(self._discriminator)
                self._PQC2.set_discriminator(self._discriminator)

                g_loss1_mean.append(0.0)
                g_loss2_mean.append(0.0)

                for _ in range(2) :
                    # 2. Train PQC1
                    g_loss1_mean[-1] += self._PQC1.train(quantum_instance, real_labels, qitype = qitype, shots =  shots)/2.

                    # 3. Train PQC2
                    g_loss2_mean[-1] += self._PQC2.train(quantum_instance, real_labels, qitype = qitype, shots =  shots)/2.

                with open("gen1_weights.txt", "+a") as f:
                    for x in self._PQC1._parameters:
                        f.write(str(x) + " ")
                    f.write("\n ")


                with open("gen2_weights.txt", "+a") as f:
                    for x in self._PQC2._parameters:
                        f.write(str(x) + " ")
                    f.write("\n ")


            self._d_loss.append(np.mean(d_loss_mean))
            self._g_loss1.append(np.mean(g_loss1_mean))
            self._g_loss2.append(np.mean(g_loss2_mean))

            print('Loss_D: %.4f\tLoss_G1: %.4f\t\tLoss_G2: %.4f\t time taken : %.4f min'
                  % (self._d_loss[-1], self._g_loss1[-1], self._g_loss2[-1], (time.time() - start)/60.0))

            with open("d_loss.txt", "+a") as f:
                f.write(str(self._d_loss[-1]) + " ")

            with open("g_loss1.txt", "+a") as f:
                f.write(str(self._g_loss1[-1]) + " ")

            with open("g_loss2.txt", "+a") as f:
                f.write(str(self._g_loss2[-1]) + " ")
