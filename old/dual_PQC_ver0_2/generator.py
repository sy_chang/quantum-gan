import pytket
from pytket import Circuit
from pytket.utils import counts_from_shot_table


import numpy as np

from qiskit.aqua.components.optimizers import ADAM

import time


class Generator:

    def __init__(self, num_qubits, depth, init_parameters = None):
        self._depth = depth

        self._num_qubits = num_qubits


        if init_parameters is None :
            self._parameters = np.array([np.random.normal() for _ in range((self._depth+1)*self._num_qubits)])
#             self._parameters = np.zeros((self._depth+1)*self._num_qubits)

        else :
            self._parameters = init_parameters


        self._optimizer = ADAM()


    def init_layers(self, circuit, parameters):
        def layer(c, params):

            for i in range(self._num_qubits-1):
                c.CX(i, i+1)

            for i in range(self._num_qubits):
                c.Ry(params[i], i)

        for i in range(self._num_qubits):
            circuit.Ry(parameters[0,i], i)

        for d in range(self._depth):
            layer(circuit,parameters[d,:])

        return circuit

    def set_optimizer(self, optimizer = None):
        if optimizer is None:
            self._optimizer = ADAM()
        else :
            self._optimizer = optimizer



    def set_lr(self, lr):
        self._optimizer._lr = lr


    def loss_function(self, actual, predicted):
        sum_score = 0.0
        for i in range(len(actual)):
            sum_score += (actual[i] * np.log(max(1e-15, predicted[i])) + \
                         (1.0 - actual[i]) * np.log(max(1e-15, 1.0 - predicted[i])))
        mean_sum_score = 1.0 / len(actual) * sum_score

        return -mean_sum_score


    def set_discriminator(self, discriminator):
        self._discriminator = discriminator


    def get_output(self, quantum_instance, params, qitype = "state_vector", num_outputs = 1024,
                    get_counts = True, input_state = None,seed = 1 ):

        circuit = Circuit(self._num_qubits)

        if input_state is not None :
            for j, b in enumerate(input_state):
                if b == 1:
                    circuit.X(j)

        circuit = self.init_layers(circuit, np.reshape(params, (self._depth+1, self._num_qubits)))


        quantum_instance.compile_circuit(circuit)

        if qitype is "state_vector" :

            result = quantum_instance.process_circuit(circuit)
            result = quantum_instance.get_state(result)
            probs = np.multiply(result, np.conj(result))
            probs = probs.real

            if get_counts :
                counts = np.rint(probs*num_outputs)


                if np.sum(counts) > num_outputs:
                    decimals = sorted(list(enumerate((probs*num_outputs)%1)), key=lambda x: x[1])
                    for i in range(np.sum(counts) - num_outputs) :
                        counts[decimals[i][0]] -= 1

                elif np.sum(counts) <  num_outputs :
                    decimals = sorted(list(enumerate((probs*num_outputs)%1)),
                                        key=lambda x: x[1], reverse = True)
                    for i in range(np.sum(counts) - num_outputs) :
                        counts[decimals[i][0]] += 1


                output = {tuple(map(int, np.binary_repr(s, width = self._num_qubits))) : counts[s] \
                            for s in range(len(counts))}
            else :
                output = probs

        else :

            circuit.measure_all()
            shot_handle = quantum_instance.process_circuit(circuit, n_shots = num_outputs, seed = seed)
            shots = quantum_instance.get_shots(shot_handle)
            result = counts_from_shot_table(shots)


            if get_counts :
                output = result
            else :

                output = np.zeros(2**self._num_qubits)

                for nbin, count in result.items() :
                    s = sum([nbin[i]*2**(self._num_qubits - i - 1) for i in range(self._num_qubits)])
                    output[s] = count

                output = output/num_outputs

        return output


    def get_objective_function(self, quantum_instance, labels, discriminator, qitype = "state_vector"):
        return

    def train(self,quantum_instance, labels,qitype = "state_vector",shots = 1024, seed = 1):
        self._shots = shots
        self._num_images = len(labels)

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        objective = self.get_objective_function(quantum_instance, labels, self._discriminator,qitype)

        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            initial_point=self._parameters
            )

        return loss


class Basis_Generator(Generator):
     def __init__(self, num_qubits, depth):
        super().__init__(num_qubits, depth)

    def get_basis(self, quantum_instance, params, qitype = "state_vector", num_outputs = 1024, seed = 1):
        return self.get_output(quantum_instance,params, qitype, num_outputs, get_counts = True, seed = seed)

    def set_images(self, images) :
        self._images = images


    def get_objective_function(self, quantum_instance, labels, discriminator, qitype = "state_vector"):
        def objective_function(params):

            basis = self.get_basis(quantum_instance, params, qitype, num_outputs = self._num_images, seed = self._seed)

            generated_images = np.empty((0, len(next(iter(self._images.values())))))

            for b, v in basis.items() :
                generated_images = np.vstack((generated_images, np.tile(self._images[b],(v, 1))))

            np.random.shuffle(generated_images)

            prediction_generated = discriminator.get_labels(generated_images, detach=True)

            return self.loss_function(labels, prediction_generated)

        return objective_function







class Image_Generator(Generator):
    def __init__(self, n1, n2, n, depth):
        if n1 > n2 :
            tmp = n2
            n2 = n1
            n1 = tmp

        super().__init__(n2, depth)

        self._n1 = n1

        self._n = n

    def set_basis(self, basis):
        self._basis = basis

    def get_images(self, quantum_instance, params, qitype = "state_vector", shots = 1024, seed = 1):
        input_basis = [np.binary_repr(s, width = self._n1) for s in range(2**self._n1)]

        output_images = dict()


        for b in input_basis :

            image = self.get_output(quantum_instance, params, qitype, num_outputs = shots, get_counts = False,
                                    input_state= b, seed = seed)

            image = image[0:2**self._n]/np.sum(image[0:2**self._n])

            output_images[b] = image

        return output_images

    def get_objective_function(self, quantum_instance, labels, discriminator, basis, qitype = "state_vector"):
        def objective_function(params):
            generated_image = self.get_output(quantum_instance, params, qitype, num_outputs = self._shots, get_counts = False,
                                    input_state= basis, seed = self._seed)
            generated_image = generated_image[0:2**self._n]/np.sum(generated_image[0:2**self._n])
            prediction_generated = discriminator.get_labels(generated_image, detach=True)
            prediction_generated.flatten()
            return self.loss_function(labels, prediction_generated)

        return objective_function

    def train(self,quantum_instance, labels, qitype = "state_vector",shots = 1024, seed = 1):
        losses = []

        for b in self._basis.keys():
            self._shots = shots
            self._num_images = len(labels)

            self._optimizer._maxiter = 1
            self._optimizer._t = 0

            self._seed = seed

            if self._discriminator is None :
                Exception("No discriminator for the generator")

            objective = self.get_objective_function(quantum_instance, np.array([labels[0]]), self._discriminator, b, qitype)

            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                initial_point=self._parameters
                )

            losses.append(loss)

        return np.mean(np.array(losses))
