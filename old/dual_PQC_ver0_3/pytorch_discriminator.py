import torch

import random
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils

from torch.autograd import Variable

import numpy as np


# Discriminator network
class Discriminator_Net(nn.Module) :
    def __init__(self, nn_architecture):
        super(Discriminator_Net, self).__init__()


        modules = []

        # Construct discriminator
        for d in nn_architecture :
            modules.append(nn.Linear(d['input_dim'], d['output_dim']))

            if d['activation'] == 'sigmoid':
                modules.append(nn.Sigmoid())
            elif d['activation'] == 'leaky_relu':
                modules.append(nn.LeakyReLU(0.2))
            elif d['activation'] == 'relu':
                modules.append(nn.ReLU())
            else :
                raise Exception('Non-supported activation function')

        self._main  = nn.Sequential(*modules)

    def forward(self, input_data):
        return self._main(input_data)


class Discriminator:
    def __init__(self, nn_architecture, lr = 0.01):
        self._netD = Discriminator_Net(nn_architecture)

        # Adam optimizer by default
        self._optimizer = optim.Adam(self._netD.parameters(), lr=lr, betas=(0.9, 0.999))


    # Get labels
    def get_labels(self, images, detach = False):
        if isinstance(images, torch.Tensor):
            pass
        else:
            images = torch.tensor(images, dtype=torch.float32)
            images = Variable(images)

        if detach :
            predicted = self._netD(images.detach())
            predicted = predicted.detach().numpy()

            return np.reshape(predicted, (len(predicted), ))

        else :
            return self._netD(images)

    # Loss function (Common BCE loss)
    def loss(self, x,y, weights = None) :
        if weights is None :
            loss_funct = nn.BCELoss()
        else :
            loss_funct = nn.BCELoss(weight = weights, reduction = 'sum')
        return loss_funct(x, y)

    # Set optimizer for discriminator
    def set_optimizer(self, optimizer):
        self._optimizer = optimizer

    # Gradient penalty for discriminator optimization
    # inspired by qiskit discriminator
    def gradient_penalty(self, x, lambda_=5., k=0.01, c=1.):

        if isinstance(x, torch.Tensor):
            pass
        else:
            x = torch.tensor(x, dtype=torch.float32)
            x = Variable(x)

        delta_ = torch.rand(x.size()) * c
        z = Variable(x+delta_, requires_grad=True)
        o = self.get_labels(z)

        d = torch.autograd.grad(o, z, grad_outputs=torch.ones(o.size()),
                                create_graph=True)[0].view(z.size(0), -1)

        return lambda_ * ((d.norm(p=2, dim=1) - k)**2).mean()


    def train(self, images, labels, weights = None, penalty = True):
        if isinstance(images, torch.Tensor):
            pass
        else:
            images = torch.tensor(images, dtype=torch.float32)
            images = Variable(images)

        if isinstance(labels, torch.Tensor):
            pass
        else :
            labels = torch.tensor(labels, dtype=torch.float32)

        weights = torch.tensor(weights, dtype=torch.float32)

        # Before training, set zero gradient for the network
        self._netD.zero_grad()

        # Get labels
        output = self._netD(images).view(-1)

        # Calculate loss with respect to the labels
        errD = self.loss(output, labels, weights)

        # Calculate the gradients for this batch
        errD.backward()

# ****MODIFIED*****
        # Gradient penalty
        #
        if labels[0] == 1 :
            self.gradient_penalty(images).backward()
# *****************

        # Update D
        self._optimizer.step()

        return output.detach().numpy(), errD.detach().numpy()
