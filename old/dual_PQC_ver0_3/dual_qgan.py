import autograd.numpy as np
import time

from scipy.stats import entropy


# Quantum GAN
class QGAN:

    def __init__(self, data, real_dist, n1, n2, batch_size = 1):
        self._data = data # data to be trained
        self._n1 = n1 # Number of qubits for PQC1
        self._n2 = n2 # Number of qubits for PQC2
        self._real_dist = real_dist

        self._g_loss1 = [] # PQC1 loss
        self._g_loss2 = [] # PQC2 loss
        self._d_loss = [] # Discriminator loss
        self._rel_entr = [] # Relative entropy

        self._batch_size = batch_size # Batch size


    # Set PQC1
    def set_PQC1(self, g_circuit):
        self._PQC1 = g_circuit
        return self._PQC1._parameters

    # Set PQC2
    def set_PQC2(self, g_circuit):
        self._PQC2 = g_circuit
        return self._PQC2._parameters

    # Set discriminator
    def set_discriminator(self, discriminator):
        self._discriminator = discriminator

    # Train qgan model
    def train(self, quantum_instance, num_epochs, qitype = "state_vector", shots = 1000, path = ""):
        for epoch in range(num_epochs):
            print("Epoch", epoch)
            start = time.time()

            num_batches = len(self._data)//self._batch_size # Number of batches


            d_loss_mean = []
            g_loss1_mean = []
            g_loss2_mean = []
            rel_entropy = []

            for n in range(num_batches):

                # Get weights and images from PQC1 and PQC2
                weights = self._PQC1.get_weights(quantum_instance, self._PQC1._parameters, qitype, num_outputs = self._batch_size)
                images = self._PQC2.get_images(quantum_instance, self._PQC2._parameters, qitype, shots = shots)

                # Real image batch
                real_image = self._data[n*self._batch_size : (n+1)*self._batch_size]

                #1. Train Discriminator for real and fake batches separately
                real_output, d_loss_real = self._discriminator.train(real_image, np.ones(self._batch_size),
                                                                        np.ones(self._batch_size)/self._batch_size)
                fake_output, d_loss_fake = self._discriminator.train(images, np.zeros(len(images)), weights)


                d_loss_mean.append((d_loss_real + d_loss_fake)/2.)

                # Set trained discriminator for PQC1 and PQC2
                self._PQC1.set_discriminator(self._discriminator)
                self._PQC2.set_discriminator(self._discriminator)

                self._PQC1.set_images(images)
                self._PQC2.set_weights(weights)
                g_loss1_mean.append(0.0)
                g_loss2_mean.append(0.0)

                for _ in range(2) :
                    # 2. Train PQC1
                    g_loss1_mean[-1] += self._PQC1.train(quantum_instance, qitype = qitype, shots =  self._batch_size)/2.
                    weights = self._PQC1.get_weights(quantum_instance, self._PQC1._parameters, qitype, num_outputs = self._batch_size)

                    self._PQC2.set_weights(weights)  # Set new weights for PQC2

                    # 3. Train PQC2
                    g_loss2_mean[-1] += self._PQC2.train(quantum_instance, qitype = qitype, shots =  shots)/2.
                    images = self._PQC2.get_images(quantum_instance, self._PQC2._parameters, qitype, shots = shots)

                    self._PQC1.set_images(images) # Set new images for PQC1



            self._d_loss.append(np.mean(d_loss_mean))
            self._g_loss1.append(np.mean(g_loss1_mean))
            self._g_loss2.append(np.mean(g_loss2_mean))
            self._rel_entr.append(entropy(self._real_dist, weights))

            print('Loss_D: %.4f\tLoss_G1: %.4f\t tLoss_G2: %.4f\t Rel_entr: %.4f\t time taken : %.4f min'
                  % (self._d_loss[-1], self._g_loss1[-1],self._g_loss2[-1],self._rel_entr[-1], (time.time() - start)/60.0))
