import pytket
from pytket import Circuit
from pytket.utils import counts_from_shot_table


from qiskit.aqua.components.optimizers import ADAM

import autograd
import autograd.numpy as np


from qiskit.aqua import aqua_globals
from qiskit.aqua.components.optimizers.optimizer import Optimizer

import time


class Generator:
    def __init__(self, num_qubits, depth, init_parameters = None):
        self._depth = depth # Depth of PQC

        self._num_qubits = num_qubits # Number of qubits in the circuit

        # In case the parameters are not initalized, initialize them according to normal distribution
        if init_parameters is None :
            self._parameters = np.array([np.random.normal() for _ in range((self._depth+1)*self._num_qubits)])
        else :
            self._parameters = init_parameters

        # Adam optimizer by default
        self._optimizer = ADAM()

    # Consturct RY parametrized quantum circuit
    def init_layers(self, circuit, parameters):
        def layer(c, params):

            for i in range(self._num_qubits-1):
                c.CZ(i, i+1) # Use CZ gate for entanglement

            for i in range(self._num_qubits):
                c.Ry(params[i], i)

        for i in range(self._num_qubits):
            circuit.Ry(parameters[0,i], i)

        for d in range(1, self._depth+1):
            layer(circuit,parameters[d,:])

        return circuit

    # Set optimizer
    def set_optimizer(self, optimizer):
        self._optimizer = optimizer
    # Set learning rate for the optmizer
    def set_lr(self, lr):
        self._optimizer._lr = lr

    # Loss function for generator
    # Common BCE loss with label = 1
    def loss_function(self, x, weights):
        try:
            loss = (-1) * np.dot(np.log(x).transpose(), weights)
        except Exception:
            loss = (-1) * np.dot(np.log(x), weights)
        return loss.flatten()

    # Set discriminator to get labels
    def set_discriminator(self, discriminator):
        self._discriminator = discriminator

    #
    def get_output(self, quantum_instance, params, qitype = "state_vector", num_outputs = 1024, input_state = None,seed = 1 ):
        circuit = Circuit(self._num_qubits)


        if input_state is not None :
            # An equiprobable superposition of computational basis states as input
            if input_state is "uniform" :
                for i in range(self._num_qubits):
                    circuit.H(i)

            else :
                # Specific computation basis state as input
                for j, b in enumerate(input_state):
                    if b == '1':
                        circuit.X(j)

        # Construct PQC
        circuit = self.init_layers(circuit, np.reshape(params, (self._depth+1, self._num_qubits)))

        # Compile circuit
        quantum_instance.compile_circuit(circuit)

        # In case the quantum instance is a statevector simulator
        if qitype is "state_vector" :

            result = quantum_instance.process_circuit(circuit)
            result = quantum_instance.get_state(result)
            probs = np.multiply(result, np.conj(result))
            probs = probs.real

            output = probs

        else :
            circuit.measure_all()
            shot_handle = quantum_instance.process_circuit(circuit, n_shots = num_outputs, seed = seed)
            shots = quantum_instance.get_shots(shot_handle)
            result = counts_from_shot_table(shots)

            output = np.zeros(2**self._num_qubits)

            for nbin, count in result.items() :
                s = sum([nbin[i]*2**(self._num_qubits - i - 1) for i in range(self._num_qubits)])
                output[s] = count

            output = output/num_outputs

        return output


    def get_objective_function(self, quantum_instance, discriminator, qitype = "state_vector"):
        return

    # Train generator
    def train(self,quantum_instance, qitype = "state_vector",shots = 1024, seed = 1):
        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        # Objective function for optimization
        objective = self.get_objective_function(quantum_instance, self._discriminator,qitype)

        # Train parameters
        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            initial_point=self._parameters
            )

        return loss

# Generator to train the distribution over images
class Weight_Generator(Generator):
    def __init__(self, num_qubits,n, depth, init_parameters = None):
        super().__init__(num_qubits, depth, init_parameters = init_parameters)
        self._n = n

    # Set image instances (Needed to calculate generator loss)
    def set_images(self, images) :
        self._images = images

    # Return distribution over image instances
    def get_weights(self, quantum_instance, params, qitype = "state_vector", num_outputs = 1024, seed = 1):
        output = self.get_output(quantum_instance,params, qitype, num_outputs, input_state = "uniform", seed = seed)
        return output[:2**self._n]/np.sum(output[:2**self._n])

    # Objective function for optimization
    def get_objective_function(self, quantum_instance, discriminator, qitype = "state_vector"):
        def objective_function(params):

            output  = self.get_weights(quantum_instance, params, qitype, num_outputs = self._shots, seed = self._seed)

            prediction_generated = discriminator.get_labels(self._images, detach=True)
            return self.loss_function(prediction_generated, output)

        return objective_function



# Generator to train the pixel amplitudes of images
class Image_Generator(Generator):
    def __init__(self, n1, n2, n, depth, init_parameters = None):
        if n1 > n2 :
            tmp = n2
            n2 = n1
            n1 = tmp

        super().__init__(n2, depth, init_parameters = init_parameters)

        self._n1 = n1
        self._n = n

    # Set weights (distribution) for images
    def set_weights(self, weights):
        self._weights = weights

    # Get images for the computational basis
    def get_images(self, quantum_instance, params, qitype = "state_vector", shots = 1024, seed = 1):
        input_basis = [np.binary_repr(s, width = self._num_qubits) for s in range(2**self._n)]

        output_images = []


        for b in input_basis :

            image = self.get_output(quantum_instance, params, qitype, num_outputs = shots, input_state= b, seed = seed)

            image = image[0:2**self._n]/np.sum(image[0:2**self._n])

            output_images.append(image)

        return np.array(output_images)



    # Objective function for a single input state
    def single_objective_function(self, quantum_instance, discriminator, basis, qitype = "state_vector") :
        def objective_function(params):
            generated_image = self.get_output(quantum_instance, params, qitype, num_outputs = self._shots,
                                    input_state= basis, seed = self._seed)

            generated_image = generated_image[0:2**self._n]/np.sum(generated_image[0:2**self._n])

            return discriminator.get_labels(generated_image, detach=True)
        return objective_function

    # Return the function to calculate the gradient with respect to several inputs
    def get_gradient_function(self, quantum_instance, discriminator, qitype = "state_vector"):

        # Return loss function for the given weights
        # Used for gradient calculation
        def get_loss_function(weights):
            def loss(x) :
                return self.loss_function(x, weights)
            return loss

        def g_fnct(params) :

            input_basis = [np.binary_repr(s, width = self._num_qubits) for s in range(2**self._n)]
            derivatives = []
            predicted_labels = []
            for b in input_basis :

                # Objective function for the specific input
                objective_function = self.single_objective_function(quantum_instance,self._discriminator, b, qitype)

                # Calculate gradient with respect to the given input
                grad = Optimizer.wrap_function(Optimizer.gradient_num_diff,
                                                                    (objective_function, self._optimizer._eps))
                derivatives.append(grad(params))

                # Get discriminator label for the image
                output = self.get_output(quantum_instance, params, qitype, num_outputs = self._shots, input_state= b, seed = self._seed)
                prediction = self._discriminator.get_labels(output[0:2**self._n]/np.sum(output[0:2**self._n]), detach = True)
                predicted_labels.append(prediction)

            # Calculate the gradient with respect to several discriminator labels
            derivatives = np.array(derivatives)
            loss_func = get_loss_function(self._weights)
            g = autograd.grad(loss_func)
            grad = g(np.array(predicted_labels))

            return np.sum(np.array([x*y for x,y in zip(grad, derivatives)]), axis = 0)
        return g_fnct

    # Objective function used for objective function
    def global_objective_function(self, quantum_instance, discriminator, qitype = "state_vector"):
        def objective_function(params):

            # Calculate loss for images generated for the given inputs
            generated_image = self.get_images(quantum_instance, params, qitype, shots = self._shots, seed = self._seed)
            prediction_generated = discriminator.get_labels(generated_image, detach=True)
            prediction_generated.flatten()
            return self.loss_function(prediction_generated, self._weights)

        return objective_function

    # Generator training function
    def train(self,quantum_instance, qitype = "state_vector",shots = 1024, seed = 1):

        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        # Gradient function for optimization
        grad_funct = self.get_gradient_function(quantum_instance,self._discriminator, qitype)
        objective = self.global_objective_function(quantum_instance, self._discriminator, qitype)

        # Train the generator parameters
        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            gradient_function = grad_funct,
            initial_point=self._parameters
            )


        return loss
