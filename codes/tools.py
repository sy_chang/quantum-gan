from __future__ import print_function
import os

import h5py 
import numpy as np
import matplotlib.pyplot as plt

from qiskit import * 

from qiskit.providers.aer.noise import NoiseModel
import qiskit.providers.aer.noise as noise
from qiskit.ignis.mitigation.measurement import CompleteMeasFitter

from qiskit.utils import QuantumInstance


def create_folder(folder_name: str, 
                    print_outputs: bool = True):        #create folders in which the trainings progress will be saved
    dirName=folder_name
    try:
        # Create target Directory
        os.mkdir(dirName)
        if print_outputs == True:
            print("Directory " , dirName ,  " Created ") 
    except FileExistsError:
        if print_outputs == True:
            print("Directory " , dirName ,  " already exists")
    return


def build_error_matrix(flip_probabilities):
    """
    (Written by Stefan Kuhn from DESY)
    Given a list of with flip probabilities [p_q0, p_q1] for the individual
    qubits q=1...N, determine the corresponding matrix of flip probabilities.
    """
    
    # Determine the number of qubits         
    N = len(flip_probabilities)    
    
    # Iterate over the qubits from N-1 to 0 as Qiskit orders qubits according to q_{N-1}, q_{N-2}, ..., q_0
    first = True
    for q in range(N-1,-1,-1):
        # Construct a matrix for the readout error
        p = flip_probabilities[q]
        ro_matrix_qubit_q = np.zeros((2,2))
        ro_matrix_qubit_q[0,0] = 1 - p[0]
        ro_matrix_qubit_q[0,1] = p[0]
        ro_matrix_qubit_q[1,0] = p[1]
        ro_matrix_qubit_q[1,1] = 1 - p[1]
        if first:
            ro_error_tot = noise.ReadoutError(ro_matrix_qubit_q)
            first = False
        else:            
            ro_error_tot = ro_error_tot.tensor(noise.ReadoutError(ro_matrix_qubit_q))
            
    return ro_error_tot



def generate_backend(backend_name: str, 
                    error_mitigation: bool, 
                    **kwargs) -> QuantumInstance:
    """
    Generate an instance of a backend which can either be the state vector simulator, 
    the qasm simulator with various noise models

    Args: 
        backend_name (str): Quantum backend to load
        error_mitigation (bool): Apply error mitigation 
        flip_prob (list): Read out error 
        cx_error (float): Two qubit gate error 
        use_noise_model (bool): Use noise model mimicing the real quantum hardware
        readout_noise_only (bool): Retrieve only the readout noise model from the real quantum hardware
    """ 

    # Set quantum instance to run the quantum generator
    if backend_name == 'statevector_simulator':
        # The state-vector simulator
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('statevector_simulator'))
    elif backend_name == 'qasm_simulator':

        if 'flip_proba' not in kwargs and 'cx_error' not in kwargs :
            # The qasm simulator mimicing a perfect quantum computer
            quantum_instance = QuantumInstance(
                backend=Aer.get_backend('qasm_simulator'))
        else : 
            flip_probabilities = kwargs['flip_proba']
            cx_error = kwargs['cx_error']

            noise_model = noise.NoiseModel()


            if flip_probabilities is not None :
                ro_error = build_error_matrix(flip_probabilities)

                if len(flip_probabilities) == 3 : 
                    noise_model.add_readout_error(ro_error, [0,1,2])
                else : 
                    noise_model.add_all_qubit_readout_error(ro_error)

            if cx_error != 0.0 : 
                #depolarizing error
                error = noise.depolarizing_error(cx_error, 2)
                noise_model.add_all_qubit_quantum_error(error, ['cx', 'cz'])
            
            basis_gates = noise_model.basis_gates
            coupling_map = [[0, 1], [1, 0], [1, 2], [1, 3], [2, 1], [3, 1], [3, 4], [4, 3]]
            
            quantum_instance = QuantumInstance(backend=Aer.get_backend(
                'qasm_simulator'), coupling_map = coupling_map,
                 basis_gates=basis_gates, noise_model=noise_model)
            if error_mitigation : 
                quantum_instance = QuantumInstance(backend=Aer.get_backend(
                    'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model,
                    measurement_error_mitigation_cls=CompleteMeasFitter)
            else : 
                quantum_instance = QuantumInstance(backend=Aer.get_backend(
                    'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)

    else : 
        # Use the real quantum hardware
        IBMQ.load_account()


        provider = IBMQ.get_provider(hub='ibm-q-cern', group='internal', project='qgan4sim')
        hardware_backend = provider.get_backend(backend_name)

        # Use noise model
        if kwargs["use_noise_model"] : 
            if kwargs["readout_noise_only"]:
                # We mimic a noisy backend but we only keep readout errors
                noise_model = NoiseModel.from_backend(hardware_backend)
                coupling_map = hardware_backend.configuration().coupling_map
                basis_gates = noise_model.basis_gates
                # After retrieving the full noise model, we serialize it into a dictionary and delete any noise that is not readout noise
                noise_model_dict = noise_model.to_dict()
                # This dictionary should have two entries with key "errors", "x90_gates", the second one is typically empty and I have no idea what it is good for
                if ('x90_gates' in noise_model_dict) and noise_model_dict['x90_gates']:
                    warnings.warn(
                        "Entry for x_90_gates in noise model dictionary not empty!")
                # We now examine the list of dictionaries specifying the errors and only keep those which are related to readout error
                tmp = list()
                for entry in noise_model_dict['errors']:
                    if entry['type'] == 'roerror':
                        # We found an entry corresponding to a readout error, keep it
                        tmp.append(entry)
                noise_model_dict['errors'] = tmp
                noise_model = NoiseModel.from_dict(noise_model_dict)
                
            else :        
                # We mimic a noisy backend
                noise_model = NoiseModel.from_backend(hardware_backend)
                coupling_map = hardware_backend.configuration().coupling_mabip
                basis_gates = noise_model.basis_gates
                quantum_instance = QuantumInstance(backend=Aer.get_backend(
                    'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)
        else : 
            # Use real hardware            
            quantum_instance = QuantumInstance(backend = hardware_backend)

    return quantum_instance



def plot_results(X_train, num_epochs, d_loss, g_loss1, g_loss2, rel_entr, weights, images, save_folder = ""):
    """
    Plot training results
    """

    t_steps = np.arange(num_epochs)

    plt.figure(figsize=(11-3/4, 8-1/4))

    # Plot progress in loss function
    plt.subplot(221)
    t_steps = np.arange(num_epochs)
    plt.title("Progress in the loss function")
    
    plt.plot(t_steps, d_loss,
             label="D_loss", linewidth=2)
    plt.plot(t_steps, g_loss1, label="G_loss1", linewidth=2)
    plt.plot(t_steps, g_loss2, label="G_loss2", linewidth=2)
    plt.grid()
    plt.legend(loc='best')
    plt.xlabel('time steps')
    plt.ylabel('loss')

    # Plot progress w.r.t relative entropy for mean image
    plt.subplot(222)
    plt.title("Relative Entropy ")
    plt.plot(t_steps, rel_entr, lw=2)
    plt.grid()
    plt.xlabel('time steps')
    plt.ylabel('relative entropy')
    plt.yscale("log")

    # Plot the mean image
    plt.subplot(223)
    plt.title("Mean image")
    x = np.linspace(0,3, 4)
    plt.bar(x,   np.sum(np.array([w*i for w,i in zip(weights, images)]), axis = 0), width=0.8, label='Simulated')
    plt.plot(x, np.mean(X_train, axis = 0), '--o', label='Target', linewidth=2,
             markersize=12, color='tab:orange')

    plt.xlabel('Calorimeter Depth')
    plt.ylabel('Normalized Energy')
    plt.legend(loc='best')

    # Plot the individual images reproduced by PQC2
    plt.subplot(224)
    plt.title("Images")
    x = np.linspace(0,3, 4)
    plt.plot(x, np.transpose(images))

    plt.xlabel('Calorimeter Depth')
    plt.ylabel('Normalized Energy')


    # Some beautyfication and save final results
    plt.tight_layout()
    plt.savefig(os.path.join(save_folder, 'plots.pdf'))
    # plt.show()
    plt.close("all")

def load_training_data(title: str = 'C:/Users/suchang/cernbox/Documents/data/classified_data_4pixels2.h5', 
                        n_clusters: int = 4):
    """
    Load training data (Not required for inference test)

    Args: 
        title: Path to load the data
        n_clusters: Number of classes in the training data
    """
    hf = h5py.File(title, 'r')
    xd = hf.get('image_data')
    labels = hf.get('labels')

    X_train = xd[:20000, :]
    labels = labels[:20000]
    hf.close()

    train_data = [[] for _ in range(n_clusters)]

    for x, l in zip(X_train, labels):
        train_data[l].append(x)

    image_dist = np.array([len(x) for x in train_data])
    mean_images = np.array([np.mean(x, axis = 0) for x in train_data])

    tuple_data = sorted(zip(image_dist, mean_images), key = lambda x : x[0])
    image_dist = np.array([x[0] for x in tuple_data])
    mean_images = np.array([x[1] for x in tuple_data])

    image_dist = image_dist/np.sum(image_dist)


    return X_train, mean_images, image_dist


