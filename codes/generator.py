from qiskit import *
from qiskit import Aer


from qiskit.algorithms.optimizers import ADAM

import autograd.numpy as np

from math import pi

from pqc import PQC


class Generator:
    """
    Class for quantum generator which consists of a Parameterized Quantum Circuit (PQC)
    """

    def __init__(self,
                 num_qubits: int,
                 n: int,
                 depth: int,
                 init_parameters: np.array = None,
                 entanglement_map: str or list = 'linear',
                 layout: list = None):
        """
        Args: 
            num_qubits: Number of qubits inside the PQC 
            n: Variable to express the image size N (n = log2(N))
            depth: Number of layers inside the PQC
            init_parameters: Initial parameters of PQC
            entanglement_map: Entaglement strategy
            layout: Qubit labels of the quantum hardware to use
        """

        self._depth = depth

        self._num_qubits = num_qubits
        self._n = n

        self._layout = layout
        if init_parameters is None:
            self._parameters = np.array(
                [np.random.normal() for _ in range((self._depth+1)*self._num_qubits)])
        else:
            self._parameters = init_parameters

        self.init_layers = PQC(num_qubits, depth, entanglement_map)
        self._optimizer = ADAM()

    def set_optimizer(self, 
                    optimizer: qiskit.algorithms.optimizers.Optimizer =None):
        if optimizer is None:
            self._optimizer = ADAM()
        else:
            self._optimizer = optimizer

    def set_lr(self,
               lr: float):
        """
        Function to set learning rate of the training

        Args: 
            lr: learning rate to be set
        """
        self._optimizer._lr = lr

    def loss_function(self, x, weights):
        try:
            # pylint: disable=no-member
            loss = (-1) * np.dot(np.log(x).transpose(), weights)
        except Exception:  # pylint: disable=broad-except
            loss = (-1) * np.dot(np.log(x), weights)
        return loss.flatten()

    def set_discriminator(self, discriminator):
        self._discriminator = discriminator

    def get_output(self,
                   quantum_instance: qiskit.utils.QuantumInstance,
                   params: np.array,
                   SV: bool = True,
                   num_outputs: int = 1024,
                   input_state: str = None,
                   seed: int = 1) -> np.array:
        """
        Function to return the output of the quantum circuit

        Args: 
            quantum_instance: Quantum instance used to run the circuit
            params: Parameters of the PQC for which we evaluate the output
            SV: Use the theoretical statevector simulator
            num_outputs: Number of measurements for sampling
            input_state: Input state of the quantum circuit
            seed: (Only used of tket) 
        """
        if SV:
            circuit = QuantumCircuit(self._num_qubits)
        else:
            circuit = QuantumCircuit(self._num_qubits, self._num_qubits)

        if input_state is not None:
            # If the input state is uniform (used for PQC1)
            if input_state == "uniform":
                for i in range(self._num_qubits):
                    circuit.h(i)

            else:  # If the input state is a binary string (used for PQC2)
                for j, b in enumerate(input_state):
                    if b == '1':
                        circuit.x(self._n - j - 1)
        # Initialize PQC
        circuit = self.init_layers(circuit, np.reshape(
            params, (self._depth+1, self._num_qubits)))

        # If we use statevector simulator (No measurement required)
        if SV:
            result = quantum_instance.execute(circuit)
            result = result.get_statevector(circuit)

            probs = np.multiply(result, np.conj(result))
            probs = probs.real

            output = probs

        else:  # Otherwise, we need to sample the qubits
            circuit.measure(range(self._num_qubits), range(self._num_qubits))

            # Use specific qubit labels
            if self._layout is not None:
                quantum_instance.set_config(initial_layout=self._layout)
                circuit = quantum_instance.transpile(circuit)
                circuit = circuit[0]
                quantum_instance.set_config(initial_layout=None)

            quantum_instance._run_config.shots = num_outputs  # Set number of shots
            result = quantum_instance.execute(circuit)
            result = result.get_counts(circuit)

            output = np.zeros(2**self._num_qubits)

            # Create a vector of probability distribution
            for nbin, count in result.items():
                s = sum([int(nbin[i])*2**(self._num_qubits - i - 1)
                         for i in range(self._num_qubits)])
                output[s] = count

            output = output/num_outputs

        return output

    def dp_dtheta(self,
                  quantum_instance: qiskit.utils.QuantumInstance,
                  x_center: float,
                  SV: bool,
                  shots: int = None,
                  input_state: str = None,
                  seed: int = 1):
        """
        Function to calculate dp/dtheta 
        (Used for parameter shift rule)
        """

        params_plus = np.repeat(np.array([x_center]), len(x_center), axis=0)
        params_minus = np.repeat(np.array([x_center]), len(x_center), axis=0)

        for i in range(len(x_center)):
            params_plus[i, i] += pi/2
            params_minus[i, i] -= pi/2

        p_plus = []
        p_minus = []
        for x in params_plus:
            prob = self.get_output(quantum_instance, x,
                                   SV, shots, input_state, seed)
            p_plus.append(prob)

        for x in params_minus:
            prob = self.get_output(quantum_instance, x,
                                   SV, shots, input_state, seed)
            p_minus.append(prob)

        return np.transpose(np.array(p_plus) - np.array(p_minus))/2.

    def train(self,
              quantum_instance: qiskit.utils.QuantumInstance,
              SV: bool = True,
              shots: int = 1024,
              PS: int = False,
              seed: int = 1) -> float:
        """
        Function for update the parameters of PQC

        Args: 
            quantum_instance: QuantumInstance to run the circuit
            SV: Use statevector simulator
            num_outputs: number of quantum circuit measurements
            PS: Use parameter shift rule
            seed: (Only for tket)
        """

        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None:
            Exception("No discriminator for the generator")

        # Get an objective function for training
        objective = self.get_objective_function(
            quantum_instance, self._discriminator, SV)

        # If we use parameter shift rule for gradient calculation
        if PS:
            grad_fn = self.get_grad_fn(
                quantum_instance, self._discriminator, SV, shots=self._shots, input_state=None, seed=1)

            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                gradient_function=grad_fn,
                initial_point=self._parameters
            )
        else:  # If we use finite difference rule for gradient calculation
            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                initial_point=self._parameters
            )
        return loss
