''''Construction of Parameterized quantum circuit'''

from qiskit import *
from qiskit.circuit.gate import *
from qiskit.circuit.library import *
import numpy as np

def PQC(num_qubits: int,
        depth: int,
        entanglement_map: str or list = 'linear'):
    """
    Function to return a function used to construct a parameterized qunatum circuit (PQC)
    with RY rotations and CZ entanglements 

    ...

    Parameters
    ----------
    num_qubits : int
        Number of qubits in the quantum circuit
    depth : int
        Depth of the PQC
    entanglement_map : str or list
        List indicating the entanglement between the qubits inside the entanglement block     
    """

    if type(entanglement_map) == str:
        if entanglement_map == 'linear':
            entanglement_map = [[i, i+1] for i in range(num_qubits - 1)]

    if len(np.array(entanglement_map).shape) == 2:
        entanglement_map = [entanglement_map]


    def init_layers(circ: QuantumCircuit, 
                    parameters: np.array) -> QuantumCircuit:
        """
        Parameters
        ----------
        circ : QuantumCircuit
            Quantum circuit where to add the PQC
        parameters : numpy.array
            Angles of the rotation gates in PQC
        """
        for i in range(num_qubits):
            circ.ry(parameters[0, i], num_qubits - i - 1)

        for d in range(1, depth+1):
            ent_map = entanglement_map[d % len(entanglement_map)]

            for ent in ent_map:
                circ.cz(ent[1], ent[0])

            for i in range(num_qubits):
                circ.ry(parameters[d, i], num_qubits - i-1)
        return circ

    return init_layers
