from generator import Generator

import autograd 
import autograd.numpy as np
from qiskit.algorithms.optimizers import Optimizer

from numba import jit

class Image_Generator(Generator):
    def __init__(self, n1, n2,n, depth, init_parameters = None):
        if n1 > n2 :
            tmp = n2
            n2 = n1
            n1 = tmp

        super().__init__(n2,n, depth, init_parameters = init_parameters)

        self._n1 = n1


    def set_weights(self, weights):
        self._weights = weights

    def get_images(self, quantum_instance, params, SV = True, shots = 1024, seed = 1):
        input_basis = [np.binary_repr(s, width = self._n) for s in range(2**self._n)]

        output_images = []


        for b in input_basis :

            image = self.get_output(quantum_instance, params, SV, num_outputs = shots, input_state= b, seed = seed)

            image = image[0:2**self._n]/np.sum(image[0:2**self._n])

            output_images.append(image)

        return np.array(output_images)


    def get_gradient_function2(self, quantum_instance, discriminator, SV = True, PS = True):

        def get_loss_function(weights):
            def loss(x) :
                return self.loss_function(x, weights)
            return loss

        def return_prob(dp, p):
            sum_p = np.sum(p[:2**self._n])
            sum_dp = np.sum(dp[:2**self._n, :], axis = 0)
            m,n = dp.shape

            dp2 = [[(sum_p - p[i])*dp[i,j] - p[i]*(sum_dp[j] - dp[i,j]) for j in range(n)] for i in range(2**self._n)]


            return np.array(dp2)/sum_p**2

        def g_fnct(params) :

            input_basis = [np.binary_repr(s, width = self._n) for s in range(2**self._n)]
            derivatives = []
            predicted_labels = []
            for b in input_basis :

                
                output = self.get_output(quantum_instance, params, SV, num_outputs = self._shots, input_state= b)
                prediction = discriminator.get_labels(output[0:2**self._n]/np.sum(output[0:2**self._n]), detach = True)
                predicted_labels.append(prediction)

                if PS : 
                    dp_dtheta = self.dp_dtheta( quantum_instance, params, SV,  self._shots, b, self._seed)
                    dp_dtheta = return_prob(dp_dtheta, output)
                    grad1 = discriminator.get_grad(output[0:2**self._n]/np.sum(output[0:2**self._n]))
                    grad1 = grad1[0].detach().numpy()
                    grad = np.sum(np.array([grad1[i]*dp_dtheta[i] for i in range(2**self._n)]), axis = 0)
                else : 
                    objective_function = self.single_objective_function(quantum_instance,discriminator, b, SV)

                    grad_fn = Optimizer.wrap_function(Optimizer.gradient_num_diff,
                                                                    (objective_function, self._optimizer._eps))
                    grad = grad_fn(params)

                derivatives.append(grad)

                
                # if PS : 
                #     dp_dtheta = dP_dTheta(PQC, quantum_instance, params, SV,  shots2, b, PQC._seed)
                #     dp_dtheta = return_prob(dp_dtheta, output)
                    
                # else : 
                #     objective_function = single_objective_function(PQC, quantum_instance,discriminator, b, SV)

                #     grad_fn = Optimizer.wrap_function(Optimizer.gradient_num_diff,
                #                                                   (objective_function, PQC._optimizer._eps))
                #     dp_dtheta = np.transpose(grad_fn(params))
                # # print(grad)
                # grad1 = discriminator.get_grad(output[0:2**PQC._n]/np.sum(output[0:2**PQC._n]))
                # grad1 = grad1[0].detach().numpy()
                #     # print(dp_dtheta.shape)
                # grad = np.sum(np.array([grad1[i]*dp_dtheta[i] for i in range(2**PQC._n)]), axis = 0)
                # derivatives.append(grad)

            derivatives = np.array(derivatives)
            loss_func = get_loss_function(self._weights)
            g = autograd.grad(loss_func)
            grad = g(np.array(predicted_labels))
            return np.sum(np.array([x*y for x,y in zip(grad, derivatives)]), axis = 0)

        return g_fnct



    def single_objective_function(self, quantum_instance, discriminator, basis, SV = True) :
        def objective_function(params):
            generated_image = self.get_output(quantum_instance, params, SV, num_outputs = self._shots,
                                    input_state= basis, seed = self._seed)

            generated_image = generated_image[0:2**self._n]/np.sum(generated_image[0:2**self._n])

            return discriminator.get_labels(generated_image, detach=True)
        return objective_function

    def get_gradient_function(self, quantum_instance, discriminator, SV = True, PS = True):

        def get_loss_function(weights):
            def loss(x) :
                return self.loss_function(x, weights)
            return loss

        def g_fnct(params) :

            input_basis = [np.binary_repr(s, width = self._n) for s in range(2**self._n)]
            derivatives = []
            predicted_labels = []
            for b in input_basis :

                
                output = self.get_output(quantum_instance, params, SV, num_outputs = self._shots, input_state= b, seed = self._seed)

                prediction = discriminator.get_labels(output[0:2**self._n]/np.sum(output[0:2**self._n]), detach = True)
                predicted_labels.append(prediction)


                if PS : 
                    dp_dtheta = self.dp_dtheta(quantum_instance, params, SV, self._shots, b, self._seed)
                    grad1 = discriminator.get_grad(output[0:2**self._n]/np.sum(output[0:2**self._n]))
                    grad1 = grad1[0].detach().numpy()

                    grad = np.sum(np.array([grad1[i]*dp_dtheta[i] for i in range(2**self._n)]), axis = 0)

                else : 
                    objective_function = self.single_objective_function(quantum_instance,discriminator, b, SV)

                    grad_fn = Optimizer.wrap_function(Optimizer.gradient_num_diff,
                                                                    (objective_function, self._optimizer._eps))
                    grad = grad_fn(params)

                derivatives.append(grad)

                

            derivatives = np.array(derivatives)
            loss_func = get_loss_function(self._weights)
            g = autograd.grad(loss_func)
            grad = g(np.array(predicted_labels))

            return np.sum(np.array([x*y for x,y in zip(grad, derivatives)]), axis = 0)
        return g_fnct

    def global_objective_function(self, quantum_instance, discriminator, SV = True):
        def objective_function(params):

            generated_image = self.get_images(quantum_instance, params, SV, shots = self._shots, seed = self._seed)
            prediction_generated = discriminator.get_labels(generated_image, detach=True)
            prediction_generated.flatten()
            return self.loss_function(prediction_generated, self._weights)

        return objective_function

    def train(self,quantum_instance, SV = True,shots = 1024, PS = True, seed = 1):
        losses = []

        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")
        grad_funct = self.get_gradient_function2(quantum_instance,self._discriminator, SV, PS)

        objective = self.global_objective_function(quantum_instance, self._discriminator, SV)

        self._parameters, loss, _ = self._optimizer.optimize(
            num_vars=len(self._parameters),
            objective_function=objective,
            gradient_function = grad_funct,
            initial_point=self._parameters
            )

        losses.append(loss)

        return np.mean(np.array(losses))
