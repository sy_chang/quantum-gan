from qiskit import *
from qiskit import Aer


from qiskit.algorithms.optimizers import ADAM


import autograd
import autograd.numpy as np

from math import pi

from qiskit.algorithms.optimizers import Optimizer

import time


class Generator:
    def __init__(self, num_qubits, n,  depth, init_parameters = None):
        self._depth = depth

        self._num_qubits = num_qubits
        self._n = n

        if init_parameters is None :
            self._parameters = np.array([np.random.normal() for _ in range((self._depth+1)*self._num_qubits)])
#             self._parameters = np.zeros((self._depth+1)*self._num_qubits)

        else :
            self._parameters = init_parameters


        self._optimizer = ADAM()

    def init_layers(self, circuit, parameters):
        def layer(c, params):

            for i in range(self._num_qubits-1):
                c.cz(i+1, i)

            for i in range(self._num_qubits):
                c.ry(params[i], self._num_qubits -i-1)

        for i in range(self._num_qubits):
            circuit.ry(parameters[0,i], self._num_qubits - i - 1)

        for d in range(1, self._depth+1):
            layer(circuit,parameters[d,:])

        return circuit

    def set_optimizer(self, optimizer = None):
        if optimizer is None:
            self._optimizer = ADAM()
        else :
            self._optimizer = optimizer



    def set_lr(self, lr):
        self._optimizer._lr = lr

    def loss_function(self, x, weights):
        try:
            # pylint: disable=no-member
            loss = (-1) * np.dot(np.log(x).transpose(), weights)
        except Exception:  # pylint: disable=broad-except
            loss = (-1) * np.dot(np.log(x), weights)
        return loss.flatten()

    def set_discriminator(self, discriminator):
        self._discriminator = discriminator


    def get_output(self, quantum_instance, params, SV = True, num_outputs = 1024, input_state = None,seed = 1 ):

        if SV:
            circuit = QuantumCircuit(self._num_qubits)
        else :
            circuit = QuantumCircuit(self._num_qubits, self._num_qubits)

        if input_state is not None :
            if input_state == "uniform" :
                for i in range(self._num_qubits):
                    circuit.h(i)

            else :
                for j, b in enumerate(input_state):
                    if b == '1':
                        circuit.x(self._n - j - 1)

        circuit = self.init_layers(circuit, np.reshape(params, (self._depth+1, self._num_qubits)))


        if SV :
            # job = execute(circuit, quantum_instance)
            # result = job.result()
            result = quantum_instance.execute(circuit)
            result = result.get_statevector(circuit)

            probs = np.multiply(result, np.conj(result))
            probs = probs.real

            output = probs

        else :
            circuit.measure(range(self._num_qubits),range(self._num_qubits))

            # job = execute(circuit, quantum_instance, shots = num_outputs)
            # result = job.result()
            quantum_instance._run_config.shots = num_outputs;
            result = quantum_instance.execute(circuit)
            result = result.get_counts(circuit)


            output = np.zeros(2**self._num_qubits)

            for nbin, count in result.items() :
                s = sum([int(nbin[i])*2**(self._num_qubits - i - 1) for i in range(self._num_qubits)])
                output[s] = count

            output = output/num_outputs

        return output

    def dp_dtheta(self, quantum_instance, x_center, SV, shots = None, input_state = None, seed = 1):
        params_plus = np.repeat(np.array([x_center]), len(x_center), axis = 0)
        params_minus = np.repeat(np.array([x_center]), len(x_center), axis = 0)

        for i in range(len(x_center)):
            params_plus[i,i] += pi/2
            params_minus[i,i] -= pi/2


        p_plus = []
        p_minus = []
        for x in params_plus :
            prob= self.get_output(quantum_instance, x, SV, shots, input_state, seed)
            p_plus.append(prob)

        for x in params_minus:
            prob= self.get_output(quantum_instance, x, SV, shots, input_state, seed)
            p_minus.append(prob)

        return np.transpose(np.array(p_plus) - np.array(p_minus))/2.
        

    def train(self,quantum_instance, SV = True,shots = 1024, PS = False, seed = 1):
        self._shots = shots

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        objective = self.get_objective_function(quantum_instance, self._discriminator,SV)

        if PS :
            grad_fn = self.get_grad_fn(quantum_instance, self._discriminator, SV, shots = self._shots, input_state = None, seed = 1)

            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                gradient_function = grad_fn,
                initial_point=self._parameters
                )
        else :
            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                initial_point=self._parameters
                )
        return loss




