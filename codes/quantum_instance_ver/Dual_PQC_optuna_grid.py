#!/usr/bin/env python
# coding: utf-8


import os
os.path.abspath(os.getcwd())

#set parameters
general_save_folder = "Search_1"         #Name of the Folder in which the results schould be saved
nb_trials = 20   #how many hyperparameter searches to run

nevt = 10000    #Number of events for training

from FunctionsQ_v1 import *      #helper functions


import numpy as np

import matplotlib.pyplot as plt

import h5py 
from math import *
from scipy.stats import entropy

import time
import csv

import os

from torch import optim
from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit
from qiskit.algorithms.optimizers import Optimizer, ADAM
from qiskit.circuit.library import  TwoLocal, UniformDistribution, NormalDistribution
from qiskit.aqua import aqua_globals

from qiskit_machine_learning.algorithms.distribution_learners.qgan import QGAN


from pytorch_discriminator import Discriminator
from basis_generator import Basis_Generator
from image_generator import Image_Generator
from qiskit.utils import algorithm_globals, QuantumInstance


from qiskit import BasicAer
import warnings


# In[5]:


#Directory to save the training data
#Leave it None, if not want to store data
create_folder(general_save_folder+'/', print_outputs = False)
create_folder(general_save_folder+'/'+ "snapshot/", print_outputs = False)
snapshot_dir = str(general_save_folder) + "/snapshot"


# In[6]:


X_train = load_training_data("/data/suchang/data/classified_data_4pixels2.h5")


# ### Initialize the qGAN
# The qGAN constructed by IBM uses a quantum generator $G_{\theta}$, a variational quantum circuit, and a classical discriminator $D_{\phi}$, a neural network. <br/>
# 
# The generator is parameterized via variational form, which consists of alternating layers of single qubit Pauli-rotations responsible for linear transformations and a set of two-qubit gates for non-linear entanglement. The choice for rotation and entanglement gates can vary, but this code uses quantum generator with Pauli-Y -rotation (RY rotation) and CZ two-qubit gates following the original QGAN
# implementation. In this case, the variational form of depth $k$ and $n$ qubits depends on $(k + 1)n$ variables, which correspond the angles for Ry rotations. 
# 
# Meanwhile, it makes use of a linear entanglement map, where CZ gates are applied from qubit $i$ to qubit $(i + 1)$, $i ∈ {0, ..., n − 2}$ (no entanglement between qubit $0$ and qubit $n − 1$). Note that using more qubits allows higher resolution of training samples and increasing the depth enables to construct more complex structure.

# In[7]:


#learningrate decay 
def l_dec(initial_lrate, epoch, start_decay=500, decay_rate=0.001):
    epoch = epoch - 1 #because training starts at epoch 1
    if epoch < start_decay:
        k = 0.0
    else:
        k = decay_rate #0.07
        epoch = epoch - start_decay
    lrate = initial_lrate * np.exp(-k*epoch)
    return lrate

# In[8]:



#Discriminator architecture
nn_architecture = [{"input_dim": 4, "output_dim": 256, "activation": "leaky_relu"},
                    {"input_dim": 256, "output_dim": 128, "activation": "leaky_relu"},
                            {"input_dim": 128, "output_dim": 1, "activation": "sigmoid"}
                        ]




PS = True
SV = False 

depth1 = 2
depth2 =  5

# Number of qubits of PQCs (n2 > n1)
n1 = 2
n2 = 4

n = 2

#Number of epochs
num_epochs = 1000


#Batch size
batch_size = 2000


#N shots
shots1 = 1000
shots2 = 1000

quantum_instance = generate_backend("qasm_simulator")


# In[11]:


def objective(trial, num_epochs, general_save_folder): #objective function: for what is optimized during the scan
    #############################

    # Parameterized quantum circuits
    PQC1 = Basis_Generator(n1, n, depth1, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n1*(depth1 + 1))])
    PQC2 = Image_Generator(n1, n2, n, depth2, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n2*(depth2 + 1))])

    PQC1.set_optimizer(ADAM(maxiter=1, tol=1e-6, beta_1=0.7,
                                beta_2=0.99, noise_factor=1e-6,
                                eps=1e-6, amsgrad=True))

    PQC2.set_optimizer(ADAM(maxiter=1, tol=1e-6, beta_1=0.7,
                                beta_2=0.99, noise_factor=1e-6,
                                eps=1e-6, amsgrad=True))

    discriminator = Discriminator(nn_architecture, lr = 1e-4)
    discriminator.set_optimizer(optim.Adam(discriminator._netD.parameters(), amsgrad = True))

    PQC1.set_discriminator(discriminator)
    PQC2.set_discriminator(discriminator)

    #############################
    #added code to initial QGAN script
    
    lrate_g1 = trial.suggest_float('lrate_g1', 0.00001, 0.001)
    lrate_g2 = trial.suggest_float('lrate_g2', 0.00001, 0.001)        
    lrate_d = trial.suggest_float('lrate_d', 0.00001, 0.001)  

    decay_rate = trial.suggest_float('decay_rate', 0.0001, 0.005)   
    start_decay = 100     #at which step you want to start lr decay: 1 epoch = 20 steps

    print("Trial Number:  ", trial.number)

    trial_folder = "/Trial_" + str(trial.number+1) + "/"
    save_folder = general_save_folder + trial_folder
    create_folder(general_save_folder+'/', print_outputs = False)
    create_folder(save_folder+'/', print_outputs = False)

    with open(os.path.join(save_folder, 'output.csv'), mode='w') as csv_file:
        fieldnames = ['epoch', 'd_loss', 'g_loss1', 'g_loss2',
                              'g_params1', 'g_params2', 'prob', 'mean_image', 'rel_entropy']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()


    f= open(save_folder + "/Parameters.txt","w")
    
    f.write("Trial: " + str(trial.number+1)  +
           "  lr G1: " +  str(np.round(lrate_g1,5))  + "  lr G2: " +  str(np.round(lrate_g2,5))  + 
            "  lr D: " + str(np.round(lrate_d,5)) + "  lr Decay: " 
            + str(np.round(decay_rate,5)) + " \n")

    f.close()
    
    ###########################################################################################################
    #from here on the normal script
    d_loss = []
    g_loss1 = []
    g_loss2 = []
    rel_entr = []
    
    start = time.time()
        
        
    num_batches = len(X_train)//batch_size
    print("Num_batches = ", num_batches)

    start = time.time()

    for epoch in range(num_epochs):
        print("Epoch", epoch)
        start_epoch = time.time()


        real_labels = np.ones(batch_size)
        fake_labels = np.zeros(batch_size)



        d_loss_mean = []
        g_loss1_mean = []
        g_loss2_mean = []
        rel_entropy = []

        np.random.shuffle(X_train)

        
        algorithm_globals.random.shuffle(X_train)
        index = 0
        
        #lr decay
        PQC1._optimizer._lr = l_dec(lrate_g1, epoch, start_decay=start_decay, decay_rate=decay_rate)
        PQC2._optimizer._lr = l_dec(lrate_g2, epoch, start_decay=start_decay, decay_rate=decay_rate)
        discriminator._optimizer._lr = l_dec(lrate_d, epoch, start_decay=start_decay, decay_rate=decay_rate)


        for b in range(num_batches):
            weights = PQC1.get_weights(quantum_instance, PQC1._parameters, SV, num_outputs = shots1)

            images = PQC2.get_images(quantum_instance, PQC2._parameters, SV, shots = shots2)

            if np.isnan(images).any() : 
                 break; 

            real_image = X_train[b*batch_size : (b+1)*batch_size]

            #1. Train Discriminator
            real_output, d_loss_real = discriminator.train(real_image, real_labels, np.ones(batch_size)/batch_size)
            fake_output, d_loss_fake = discriminator.train(images, np.zeros(len(images)), weights)

#                 print(np.mean(fake_output))
            d_loss_mean.append((d_loss_real + d_loss_fake)/2.)

            PQC1.set_discriminator(discriminator)
            PQC2.set_discriminator(discriminator)

            PQC1.set_images(images)
            PQC2.set_weights(weights)
            g_loss1_mean.append(0.0)
            g_loss2_mean.append(0.0)

            for _ in range(2) :
                # 2. Train PQC1
                g_loss1_mean[-1] += PQC1.train(quantum_instance, SV, num_outputs = shots1, PS = PS)/2.
                weights = PQC1.get_weights(quantum_instance, PQC1._parameters, SV, num_outputs = shots1)

                PQC2.set_weights(weights)

                g_loss2_mean[-1] += PQC2.train(quantum_instance, SV, shots = shots2, PS = PS)/2.
                images = PQC2.get_images(quantum_instance, PQC2._parameters, SV, shots = shots2)

                PQC1.set_images(images)

            if np.isnan(d_loss_mean[-1]) or np.isnan(g_loss1_mean[-1]) or np.isnan(g_loss2_mean[-1]):
                break;

        d_loss.append(np.mean(d_loss_mean))
        g_loss1.append(np.mean(g_loss1_mean))
        g_loss2.append(np.mean(g_loss2_mean))
        rel_entr.append(entropy(np.mean(X_train, axis = 0), np.sum(np.array([w*i for w,i in zip(weights, images)]), axis = 0)))

        print('Loss_D: %.4f\tLoss_G1: %.4f\t tLoss_G2: %.4f\t Rel_entr: %.4f\t time taken : %.4f min'
                % (d_loss[-1], g_loss1[-1], g_loss2[-1], rel_entr[-1], (time.time() - start)/60.0))

        with open(os.path.join(save_folder, 'output.csv'), mode='a') as csv_file:
            fieldnames = ['epoch', 'd_loss', 'g_loss1', 'g_loss2',
                            'g_params1', 'g_params2', 'prob', 'mean_image', 'rel_entropy']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writerow({'epoch': epoch, 'd_loss': np.around(d_loss[-1], 5),
                                'g_loss1': np.around(g_loss1[-1], 5),
                                'g_loss2': np.around(g_loss2[-1], 5),
                                'g_params1': PQC1._parameters,
                                'g_params2' : PQC2._parameters,
                                'prob' : weights,
                                'mean_image' : images,
                                'rel_entropy': np.around(rel_entr[-1], 5)})
        # discriminator.save_model(save_folder)  # Store discriminator model            
            
        end_epoch = time.time()
        print('epoch runtime: ', np.round((end_epoch - start_epoch)/60.,3), ' min')
        if np.isnan(images).any() or g_loss1[-1] > 3 or np.isnan(d_loss_mean[-1]) or np.isnan(g_loss1_mean[-1]) or np.isnan(g_loss2_mean[-1]): 
                break 

    end = time.time()
    # Runtime
    print('qGAN training runtime: ', (end - start)/60., ' min')
    plot_results(X_train, len(g_loss1), d_loss, g_loss1, g_loss2, rel_entr, weights, images, save_folder = save_folder)
    
    #################################################################################################################
    #here stops the normal script    
    
    f= open(general_save_folder + "/Intermediate_Summary.txt","a")
    f.write("Trial: " + str(trial.number+1) + "     Relative Entropy: " + str(np.round(rel_entr[-1],5)) +  
           "    lr G1: " + str(np.round(lrate_g1,5)) +  
           "    lr G2: " + str(np.round(lrate_g2,5)) + "  lr D: " + str(np.round(lrate_d,5)) + "  lr Decay: " 
            + str(np.round(decay_rate,5))+ " \n" )
    f.close()
    #dump study
    pickle.dump(study, open(general_save_folder + '/study.pkl', 'wb'))
    return rel_entr[-1]






# In[12]:


import optuna
#https://optuna.readthedocs.io/en/latest/reference/generated/optuna.samplers.TPESampler.html
#theory of tpe: https://docs.openvinotoolkit.org/latest/pot_compression_optimization_tpe_README.html

#python_search = True   #if true then it loads a previously created study by the file "create_study.py"
python_search = False   #if false then it starts a new study


lrate_g1 = [0.0001, 0.0005, 0.001]
lrate_g2 = [0.0001, 0.0005, 0.001]
lrate_d = [ 0.0001, 0.0005, 0.001]
decay_rate = [0.0005, 0.001, 0.0005]

nb_trials = len(lrate_g1) * len(lrate_g2) * len(lrate_d) * len(decay_rate)
print(len(lrate_g1), len(lrate_g2), len(lrate_d), len(decay_rate), nb_trials)

# Non-int points are specified in the grid.
search_space = {"lrate_g1": lrate_g1, "lrate_g2" : lrate_g2, "lrate_d": lrate_d, "decay_rate": decay_rate}

optuna_optimizer = optuna.samplers.GridSampler(search_space)
if python_search == True:
    study = optuna.load_study(study_name="dist_opt", storage='sqlite:///opt1.db', sampler=optuna_optimizer)
else:
    study = optuna.create_study(direction = "minimize", sampler=optuna.samplers.GridSampler(search_space))                     #study is the whole otimization process; a set of trials
study.optimize(lambda trial: objective(trial, num_epochs, general_save_folder), 
               n_trials=nb_trials)

study.best_params  # E.g. {'x': 2.002108042}


# In[13]:


f= open(general_save_folder + "/Summary.txt","w")
#f.write("\n" + str(study.best_params))
#f.write("\n\n" + str(study.best_trial) + "\n\n")
f.write("Best Parameters:")
f.write("\nRun Number: " + str(study.best_trial.number) + "   Accuracy: "+ str(study.best_trial.value)+ "   Parameters: "+ str(study.best_trial.params) +  "\n\n")
f.write("All Runs:")
for nb in range(len(study.trials)):
    f.write("\n" + str(study.trials[nb]))
f.close()


# In[ ]:




