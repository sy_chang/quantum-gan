#!/usr/bin/env python
# coding: utf-8

# In[1]:


from basis_generator import Basis_Generator
from image_generator import  Image_Generator
from pytorch_discriminator import Discriminator
import h5py
import warnings
import argparse
import os
import csv
import time

from qiskit import *
from qiskit import Aer

from qiskit.providers.aer.noise import NoiseModel
import qiskit.providers.aer.noise as noise


from qiskit.algorithms.optimizers import ADAM, Optimizer
from qiskit.utils import algorithm_globals

import torch.optim as optim

import autograd.numpy as np
from math import floor

import torch
import matplotlib.pyplot as plt

from FunctionsQ_v1 import *

import autograd

from scipy.stats import entropy


# In[5]:


def l_dec(initial_lrate, epoch, start_decay=500, decay_rate=0.001):
    epoch = epoch - 1 #because training starts at epoch 1
    if epoch < start_decay:
        k = 0.0
    else:
        k = decay_rate #0.07
        epoch = epoch - start_decay
    lrate = initial_lrate * np.exp(-k*epoch)
    return lrate


def train(X_train, PQC1, PQC2, discriminator, num_epochs, quantum_instance, 
             lrate_g1 = 1E-3, lrate_g2 = 1E-4, lrate_d = 1E-4, decay_rate = 0, shots1 = 1000, shots2 = 1000, SV = True, PS = False) : 
    ###########################################################################################################
    #from here on the normal script
    d_loss = []
    g_loss1 = []
    g_loss2 = []
    rel_entr = []
    start = time.time()
        
    start_decay = 100

    num_batches = len(X_train)//batch_size
    print("Num_batches = ", num_batches)

    start = time.time()

    for epoch in range(num_epochs):
        print("Epoch", epoch)
        start_epoch = time.time()


        real_labels = np.ones(batch_size)
        fake_labels = np.zeros(batch_size)



        d_loss_mean = []
        g_loss1_mean = []
        g_loss2_mean = []
        rel_entropy = []

        np.random.shuffle(X_train)

        
        algorithm_globals.random.shuffle(X_train)
        index = 0
        
        #lr decay
        PQC1._optimizer._lr = l_dec(lrate_g1, epoch, start_decay=start_decay, decay_rate=decay_rate)
        PQC2._optimizer._lr = l_dec(lrate_g2, epoch, start_decay=start_decay, decay_rate=decay_rate)
        discriminator._optimizer._lr = l_dec(lrate_d, epoch, start_decay=start_decay, decay_rate=decay_rate)


        for b in range(num_batches):
            weights = PQC1.get_weights(quantum_instance, PQC1._parameters, SV, num_outputs = shots1)

            images = PQC2.get_images(quantum_instance, PQC2._parameters, SV, shots = shots2)


            real_image = X_train[b*batch_size : (b+1)*batch_size]

            #1. Train Discriminator
            real_output, d_loss_real = discriminator.train(real_image, real_labels, np.ones(batch_size)/batch_size)
            fake_output, d_loss_fake = discriminator.train(images, np.zeros(len(images)), weights)

    #                 print(np.mean(fake_output))
            d_loss_mean.append((d_loss_real + d_loss_fake)/2.)

            PQC1.set_discriminator(discriminator)
            PQC2.set_discriminator(discriminator)

            PQC1.set_images(images)
            PQC2.set_weights(weights)
            g_loss1_mean.append(0.0)
            g_loss2_mean.append(0.0)

            for _ in range(2) :
                # 2. Train PQC1
                g_loss1_mean[-1] += PQC1.train(quantum_instance, SV, num_outputs = shots1, PS = PS)/2.
                weights = PQC1.get_weights(quantum_instance, PQC1._parameters, SV, num_outputs = shots1)
                PQC2.set_weights(weights)

                g_loss2_mean[-1] += PQC2.train(quantum_instance, SV, shots = shots2, PS = PS)/2.
                images = PQC2.get_images(quantum_instance, PQC2._parameters, SV, shots = shots2)

                PQC1.set_images(images)

        d_loss.append(np.mean(d_loss_mean))
        g_loss1.append(np.mean(g_loss1_mean))
        g_loss2.append(np.mean(g_loss2_mean))
        rel_entr.append(entropy(np.mean(X_train, axis = 0), np.sum(np.array([w*i for w,i in zip(weights, images)]), axis = 0)))

        print('Loss_D: %.4f\tLoss_G1: %.4f\t tLoss_G2: %.4f\t Rel_entr: %.4f\t time taken : %.4f min'
                % (d_loss[-1], g_loss1[-1], g_loss2[-1], rel_entr[-1], (time.time() - start)/60.0))

        with open(os.path.join(save_folder, 'output.csv'), mode='a') as csv_file:
            fieldnames = ['epoch', 'd_loss', 'g_loss1', 'g_loss2',
                            'g_params1', 'g_params2', 'prob', 'mean_image', 'rel_entropy']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writerow({'epoch': epoch, 'd_loss': np.around(d_loss[-1], 5),
                                'g_loss1': np.around(g_loss1[-1], 5),
                                'g_loss2': np.around(g_loss2[-1], 5),
                                'g_params1': PQC1._parameters,
                                'g_params2' : PQC2._parameters,
                                'prob' : weights,
                                'mean_image' : images,
                                'rel_entropy': np.around(rel_entr[-1], 5)})
        # discriminator.save_model(save_folder)  # Store discriminator model            
            
        end_epoch = time.time()
        print('epoch runtime: ', np.round((end_epoch - start_epoch)/60.,3), ' min')
        
    end = time.time()
    # Runtime
    print('qGAN training runtime: ', (end - start)/60., ' min')
    plot_results(X_train, num_epochs, d_loss, g_loss1, g_loss2, rel_entr, weights, images, save_folder = save_folder)

    #################################################################################################################
    #here stops the normal script    

    f= open(general_save_folder + "/Intermediate_Summary.txt","a")
    f.write("Trial: " + str(trial.number+1) + "     Relative Entropy: " + str(np.round(rel_entr[-1],5)) +  
            "    lr G1: " + str(np.round(lrate_g1,5)) +  
            "    lr G2: " + str(np.round(lrate_g2,5)) + "  lr D: " + str(np.round(lrate_d,5)) + "  lr Decay: " 
            + str(np.round(decay_rate,5))+ " \n" )
    f.close()

    return g_loss1, g_loss2, d_loss, rel_entr


# In[6]:


path = ""

backend_name = "qasm_simulator"
p = 0.02

noise_model = NoiseModel()

error = noise.depolarizing_error(p, 2)
noise_model.add_all_qubit_quantum_error(error, ['cx', 'cz'])

shots1 = 1000
shots2 = 1000

num_epochs = 500

shift_rule = True
readout_noise_only = False

lr1 = 0.001 
lr2 = 0.001
lrD = 0.0001         
decay_rate = 0.001 

print("Training qGAN using")
print(" - num_epochs:        ", num_epochs)
print(" - backend:           ", backend_name)
print(" - shift_rule:        ", shift_rule)
print(" - learning_rate_pqc1: ", lr1)
print(" - learning_rate_pqc2: ", lr2)
print(" - learning_rate_disc ", lrD )
print(" - decay_rate: ", decay_rate)

if backend_name != 'statevector_simulator':
    print(" - num_shots_pqc1:         ", shots1)
    print(" - num_shots_pqc2:         ", shots2)
    print(" - only bitflip:      ", readout_noise_only)

    
  

statevector = False
if backend_name == "statevector_simulator":
    statevector = True
    
# Get a backend
quantum_instance = generate_backend(backend_name, readout_noise_only, noise_model)

   
depth1 = 2
depth2 =  5

# Number of qubits of PQCs (n2 > n1)
n1 = 2
n2 = 4

n = 2


#Batch size
batch_size = 2000

#Discriminator architecture
nn_architecture = [{"input_dim": 4, "output_dim": 256, "activation": "leaky_relu"}, 
                {"input_dim": 256, "output_dim": 128, "activation": "leaky_relu"},
                        {"input_dim": 128, "output_dim": 1, "activation": "sigmoid"}
                    ]

# Parameterized quantum circuits
PQC1 = Basis_Generator(n1, n, depth1, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n1*(depth1 + 1))])
PQC2 = Image_Generator(n1, n2, n, depth2, init_parameters = [np.random.uniform(-0.1, 0.1)*np.pi for _ in range(n2*(depth2 + 1))])


PQC1.set_optimizer(ADAM(maxiter=1, tol=1e-6, beta_1=0.7,
                            beta_2=0.99, noise_factor=1e-6,
                            eps=1e-6, amsgrad=True))

PQC2.set_optimizer(ADAM(maxiter=1, tol=1e-6, beta_1=0.7,
                            beta_2=0.99, noise_factor=1e-6,
                            eps=1e-6, amsgrad=True))

discriminator = Discriminator(nn_architecture, lr = 1e-4)
discriminator.set_optimizer(optim.Adam(discriminator._netD.parameters(), amsgrad = True))

PQC1.set_discriminator(discriminator)
PQC2.set_discriminator(discriminator)

#############################
#added code to initial QGAN script


# In[4]:


start_decay = 100     #at which step you want to start lr decay: 1 epoch = 20 steps

general_save_folder = 'results/'

count = 0
trial_folder = "/Trial_" + str(count+1) + "/"
while os.path.isdir(general_save_folder + trial_folder):
    count += 1
    trial_folder = "/Trial_" + str(count+1) + "/"

save_folder = general_save_folder + trial_folder
create_folder(general_save_folder+'/', print_outputs = False)
create_folder(save_folder+'/', print_outputs = False)

with open(os.path.join(save_folder, 'output.csv'), mode='w') as csv_file:
    fieldnames = ['epoch', 'd_loss', 'g_loss1', 'g_loss2', 
                            'g_params1', 'g_params2', 'prob', 'mean_image', 'rel_entropy']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()


f= open(save_folder + "/Parameters.txt","w")

f.write("Trial: " + str(count + 1)  +
        "  lr G1: " +  str(np.round(lr1,5))  + "  lr G2: " +  str(np.round(lr2,5))  + 
        "  lr D: " + str(np.round(lrD,5)) + "  lr Decay: " 
        + str(np.round(decay_rate,5)) + " \n")

f.close()



X_train, image_dist = load_training_data()

g_loss1, g_loss2, d_loss, rel_entr = train(X_train, PQC1, PQC2, discriminator, num_epochs, quantum_instance, lrate_g1 = lr1,  lrate_g2 = lr2,  lrate_d = lrD, decay_rate= decay_rate, shots1 = shots1, shots2 = shots2, SV = statevector, PS =shift_rule)


# In[ ]:




