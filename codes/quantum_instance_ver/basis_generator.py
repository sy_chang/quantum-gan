from generator import Generator

import autograd 
import autograd.numpy as np

from qiskit.algorithms.optimizers import Optimizer

class Basis_Generator(Generator):
    def __init__(self, num_qubits,n, depth, init_parameters = None):
        super().__init__(num_qubits, n, depth, init_parameters = init_parameters)


    def get_weights(self, quantum_instance, params, SV = True, num_outputs = 1024, seed = 1):
        output = self.get_output(quantum_instance,params, SV, num_outputs, input_state = "uniform", seed = seed)
        return output[:2**self._n]/np.sum(output[:2**self._n])

    def set_images(self, images) :
        self._images = images


    def get_grad_fn(self, quantum_instance, discriminator, SV, shots = None, seed = 1):
        def grad_fn(x_center):
            prediction_generated = discriminator.get_labels(self._images, detach=True)

            dp_dtheta = self.dp_dtheta(quantum_instance, x_center, SV, shots, 'uniform', seed)

            gradients = []
            for j in range(len(x_center)):
                gradients.append(np.sum(np.array([-dp_dtheta[i][j]
                                                  *np.log(prediction_generated[i])/2
                                                  for i in range(len(prediction_generated))])))
            return np.array(gradients)
        return grad_fn


    def get_objective_function(self, quantum_instance, discriminator,SV = True):
        def objective_function(params):

            output  = self.get_weights(quantum_instance, params, SV, num_outputs = self._shots, seed = self._seed)

            prediction_generated = discriminator.get_labels(self._images, detach=True)
            return self.loss_function(prediction_generated, output)

        return objective_function

    

    def train(self,quantum_instance, SV = True, num_outputs = 1024, PS = False, seed = 1):
        self._shots = num_outputs

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None :
            Exception("No discriminator for the generator")

        objective = self.get_objective_function(quantum_instance, self._discriminator,SV)

        if PS :
            grad_fn = self.get_grad_fn(quantum_instance, self._discriminator, SV, shots = self._shots, seed = 1)

            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                gradient_function = grad_fn,
                initial_point=self._parameters
                )
        else :
            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                initial_point=self._parameters
                )
        return loss


