from math import *

from time import time
import csv
import pandas as pd
import os

from basis_generator import Basis_Generator
from image_generator import Image_Generator

from tools import *  # helper functions

import argparse
import yaml

if __name__ == "__main__":

    # Import config file
    parser = argparse.ArgumentParser(
        description="Run Dual-PQC inference on simulator")
    parser.add_argument('--config',  '-c',
                        dest="filename",
                        metavar='FILE',
                        help='path to the config file',
                        default='inference_configs/inference1.yaml')

    args = parser.parse_args()

    with open(args.filename, 'r') as file:
        try:
            config = yaml.safe_load(file)
        except yaml.YAMLError as exc:
            print(exc)

    # Backend used for the inference
    backend_name = config['inference_params']['backend_name']
    shot = config['inference_params']['num_shots']  # Number of measurements
    # Number of simulations to be repeted
    num_runs = config['inference_params']['num_runs']
    # Epoch at which we perform inference test
    epoch = config['inference_params']['inference_epoch']

    pqc1_layout = None
    pqc2_layout = None

    # Qubit labels which we want to use
    if 'pqc1_layout' in config['inference_params']:
        pqc1_layout = config['inference_params']['pqc1_layout']  # PQC1

    if 'pqc2_layout' in config['inference_params']:
        pqc2_layout = config['inference_params']['pqc2_layout']  # PQC2

    # Factor to express size of image (n = log2(N))
    n = config['model_params']['n']
    n1 = config['model_params']['n1']  # Number of qubits in PQC1
    n2 = config['model_params']['n2']  # Number of qubits in PQC2
    depth1 = config['model_params']['depth1']  # Depth of PQC1
    depth2 = config['model_params']['depth2']  # Depth of PQC2

    pqc1_entmap = 'linear'
    pqc2_entmap = 'linear'

    # Entanglment strategies
    # Can be str or list of control and target qubit couples
    if 'pqc1_entmap' in config['model_params']:
        pqc1_entmap = config['model_params']['pqc1_entmap']  # PQC1

    if 'pqc2_entmap' in config['model_params']:
        pqc2_entmap = config['model_params']['pqc2_entmap']  # PQC2

    # Use error mitigation
    error_mitigation = config["mitigation_params"]["error_mitigation"]

    # Path from which we load the generator parameters
    load_path = config['loading_params']['load_path']

    # Path to save the results
    save_dir = config['logging_params']['save_dir']
    # File name
    save_file = config['logging_params']['save_file']

    # Load parameters
    data = pd.read_csv(load_path)

    g_params1 = data['g_params1'][epoch - 1]  # PQC1 parameters
    g_params2 = data['g_params2'][epoch - 1]  # PQC2 parameters

    g_params1 = g_params1.replace("]", ""). replace("[", "").split(" ")
    g_params1 = [float(x) for x in g_params1 if x != ""]

    g_params2 = g_params2.replace("]", ""). replace("[", "").split(" ")
    g_params2 = [float(x) for x in g_params2 if x != ""]

    # Create generators
    PQC1 = Basis_Generator(n1, n, depth1, init_parameters=g_params1, entanglement_map=pqc1_entmap,
                           layout=pqc1_layout)
    PQC2 = Image_Generator(n1, n2, n, depth2, init_parameters=g_params2, entanglement_map=pqc2_entmap,
                           layout=pqc2_layout)

    # Generate quantum_instance
    if "noise_params" in config:
        # If we use noise model
        quantum_instance = generate_backend(
            backend_name, error_mitigation, **config["noise_params"])
    else:
        quantum_instance = generate_backend(
            backend_name, error_mitigation)

    # Open file to save the resultsF
    with open(os.path.join(save_dir, save_file), mode='w') as csv_file:
        fieldnames = ['run', 'prob', 'mean_image', 'time']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()

    # Run inference test
    for run in range(num_runs):
        print("run", run)
        start_time = time()
        # PQC1
        weights = PQC1.get_weights(
            quantum_instance, g_params1, False, int(shot))
        # PQC2
        images = PQC2.get_images(quantum_instance, g_params2, False, int(shot))
        with open(os.path.join(save_dir, save_file), mode='a') as csv_file:
            fieldnames = ['run', 'prob', 'mean_image', 'time']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writerow({'run': run,
                             'prob': weights,
                             'mean_image': images,
                             'time': (time() - start_time)/60.0})
