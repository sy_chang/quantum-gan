from generator import Generator

import autograd.numpy as np
import qiskit


class Basis_Generator(Generator):
    """
    Class of the quantum generator which reproduces the probability distribution over a compuational basis
    Called as PQC1 in the paper
    """

    def __init__(self,
                 num_qubits: int,
                 n: int,
                 depth: int,
                 init_parameters: np.array = None,
                 entanglement_map: str or list = 'linear',
                 layout: list = None):
        """
        Args: 
            num_qubits: Number of qubits inside the PQC 
            n: Variable to express the image size N (n = log2(N))
            depth: Number of layers inside the PQC
            init_parameters: Initial parameters of PQC
            entanglement_map: Entaglement strategy
            layout: Qubit labels of the quantum hardware to use
        """
    
        super().__init__(num_qubits, n, depth, init_parameters=init_parameters,
                         entanglement_map=entanglement_map, layout=layout)

    def get_weights(self,
                    quantum_instance: qiskit.utils.QuantumInstance,
                    params: np.array,
                    SV: bool = True,
                    num_outputs: int = 1024,
                    seed: int = 1):
        """
        Function to generate a probability distribution over a set of computational basis

        Args: 
            quantum_instance: Quantum instance used to run the circuit
            params: Parameters of the PQC for which we evaluate the output
            SV: Use the theoretical statevector simulator
            num_outputs: Number of measurements for sampling
            seed: (Only used of tket) 
        """

        output = self.get_output(
            quantum_instance, params, SV, num_outputs, input_state="uniform", seed=seed)
        return output[:2**self._n]/np.sum(output[:2**self._n])

    def set_images(self, 
                    images: np.array):
        
        self._images = images

    def get_grad_fn(self,
                    quantum_instance: qiskit.utils.QuantumInstance,
                    discriminator,
                    SV: bool,
                    shots: int = None,
                    seed: int = 1) -> np.array:
        """
        Return a function for gradient calculation
        """
        def grad_fn(x_center):
            prediction_generated = discriminator.get_labels(
                self._images, detach=True)

            dp_dtheta = self.dp_dtheta(
                quantum_instance, x_center, SV, shots, 'uniform', seed)

            gradients = []
            for j in range(len(x_center)):
                gradients.append(np.sum(np.array([-dp_dtheta[i][j]
                                                  * np.log(prediction_generated[i])/2
                                                  for i in range(len(prediction_generated))])))
            return np.array(gradients)
        return grad_fn

    def get_objective_function(self, 
                                quantum_instance: qiskit.utils.QuantumInstance, 
                                discriminator, 
                                SV: bool=True):
        """
        Return an objective function for image reproduced from a single computational basis as input
        """
        def objective_function(params: np.array) -> float: 

            output = self.get_weights(
                quantum_instance, params, SV, num_outputs=self._shots, seed=self._seed)

            prediction_generated = discriminator.get_labels(
                self._images, detach=True)
            return self.loss_function(prediction_generated, output)

        return objective_function

    def train(self, 
                quantum_instance: qiskit.utils.QuantumInstance, 
                SV: bool=True, 
                num_outputs:int =1024, 
                PS: bool=False, 
                seed:int=1) -> float:
        """
        Function for update the parameters of PQC

        Args: 
            quantum_instance: QuantumInstance to run the circuit
            SV: Use statevector simulator
            num_outputs: number of quantum circuit measurements
            PS: Use parameter shift rule
            seed: (Only for tket)
        """


        self._shots = num_outputs

        self._optimizer._maxiter = 1
        self._optimizer._t = 0

        self._seed = seed

        if self._discriminator is None:
            Exception("No discriminator for the generator")

        # Get an objective function for training 
        objective = self.get_objective_function(
            quantum_instance, self._discriminator, SV)

        # If we use parameter shift rule for gradient calculation
        if PS:
            grad_fn = self.get_grad_fn(
                quantum_instance, self._discriminator, SV, shots=self._shots, seed=1)

            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                gradient_function=grad_fn,
                initial_point=self._parameters
            )
        else:  # If we use finite difference rule for gradient calculation
            self._parameters, loss, _ = self._optimizer.optimize(
                num_vars=len(self._parameters),
                objective_function=objective,
                initial_point=self._parameters
            )
        return loss
