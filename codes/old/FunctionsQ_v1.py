from __future__ import print_function
from collections import defaultdict
try:
    import cPickle as pickle
except ImportError:
    import pickle
import argparse
import os

from six.moves import range
import sys
import h5py 
import numpy as np
import matplotlib.pyplot as plt

from qiskit import * 

from qiskit.providers.aer.noise import NoiseModel

from qiskit.utils import QuantumInstance


def create_folder(folder_name, print_outputs = True):        #create folders in which the trainings progress will be saved
    dirName=folder_name
    try:
        # Create target Directory
        os.mkdir(dirName)
        if print_outputs == True:
            print("Directory " , dirName ,  " Created ") 
    except FileExistsError:
        if print_outputs == True:
            print("Directory " , dirName ,  " already exists")
    return


def generate_backend(backend_name, readout_noise_only=False):
    """
    Generate an instance of a backend which can either be the state vector simulator, the qasm simulator with various noise models 
    """
    # Set quantum instance to run the quantum generator
    if backend_name == 'statevector_simulator':
        # The state-vector simulator
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('statevector_simulator'))

    elif backend_name == 'qasm_simulator':
        # The qasm simulator mimicing a perfect quantum computer
        quantum_instance = QuantumInstance(
            backend=Aer.get_backend('qasm_simulator'))

    elif readout_noise_only:
        # We mimic a noisy backend but we only keep readout errors
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        # After retrieving the full noise model, we serialize it into a dictionary and delete any noise that is not readout noise
        noise_model_dict = noise_model.to_dict()
        # This dictionary should have two entries with key "errors", "x90_gates", the second one is typically empty and I have no idea what it is good for
        if ('x90_gates' in noise_model_dict) and noise_model_dict['x90_gates']:
            warnings.warn(
                "Entry for x_90_gates in noise model dictionary not empty!")
        # We now examine the list of dictionaries specifying the errors and only keep those which are related to readout error
        tmp = list()
        for entry in noise_model_dict['errors']:
            if entry['type'] == 'roerror':
                # We found an entry corresponding to a readout error, keep it
                tmp.append(entry)
        noise_model_dict['errors'] = tmp
        noise_model = NoiseModel.from_dict(noise_model_dict)
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)
    else:
        # We mimic a noisy backend
        IBMQ.load_account()
        provider = IBMQ.get_provider(
            hub='ibm-q')
        hardware_backend = provider.get_backend(backend_name)
        noise_model = NoiseModel.from_backend(hardware_backend)
        coupling_map = hardware_backend.configuration().coupling_map
        basis_gates = noise_model.basis_gates
        quantum_instance = QuantumInstance(backend=Aer.get_backend(
            'qasm_simulator'), coupling_map=coupling_map, basis_gates=basis_gates, noise_model=noise_model)

    return quantum_instance


def plot_results(X_train, num_epochs, d_loss, g_loss1, g_loss2, rel_entr, weights, images, save_folder = ""):
    "Visualize final results"

    t_steps = np.arange(num_epochs)

    plt.figure(figsize=(11-3/4, 8-1/4))

    plt.subplot(221)
    t_steps = np.arange(num_epochs)
    plt.title("Progress in the loss function")
    
    plt.plot(t_steps, d_loss,
             label="D_loss", linewidth=2)
    plt.plot(t_steps, g_loss1, label="G_loss1", linewidth=2)
    plt.plot(t_steps, g_loss2, label="G_loss2", linewidth=2)
    plt.grid()
    plt.legend(loc='best')
    plt.xlabel('time steps')
    plt.ylabel('loss')

    # Plot progress w.r.t relative entropy
    plt.subplot(222)
    plt.title("Relative Entropy ")
    plt.plot(t_steps, rel_entr, lw=2)
    plt.grid()
    plt.xlabel('time steps')
    plt.ylabel('relative entropy')
    plt.yscale("log")

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(223)
    plt.title("Mean image")
    x = np.linspace(0,3, 4)
    plt.bar(x,   np.sum(np.array([w*i for w,i in zip(weights, images)]), axis = 0), width=0.8, label='Simulated')
    plt.plot(x, np.mean(X_train, axis = 0), '--o', label='Target', linewidth=2,
             markersize=12, color='tab:orange')

    plt.xlabel('Calorimeter Depth')
    plt.ylabel('Normalized Energy')
    plt.legend(loc='best')

    # Plot the PDF of the resulting distribution against the target distribution
    plt.subplot(224)
    plt.title("Images")
    x = np.linspace(0,3, 4)
    plt.plot(x, np.transpose(images))

    plt.xlabel('Calorimeter Depth')
    plt.ylabel('Normalized Energy')


    # Some beautyfication and save final results
    plt.tight_layout()
    plt.savefig(os.path.join(save_folder, 'plots.pdf'))
    # plt.show()
    plt.close("all")

def load_training_data(title = 'C:/Users/suchang/cernbox/Documents/data/classified_data_4pixels2.h5', n_clusters = 4):
    hf = h5py.File(title, 'r')
    xd = hf.get('image_data')
    labels = hf.get('labels')

    X_train = xd[:20000, :]
    labels = labels[:20000]
    hf.close()

    train_data = [[] for _ in range(n_clusters)]

    for x, l in zip(X_train, labels):
        train_data[l].append(x)

    image_dist = np.array([len(x) for x in train_data])
    mean_images = np.array([np.mean(x, axis = 0) for x in train_data])

    tuple_data = sorted(zip(image_dist, mean_images), key = lambda x : x[0])
    image_dist = np.array([x[0] for x in tuple_data])
    mean_images = np.array([x[1] for x in tuple_data])

    image_dist = image_dist/np.sum(image_dist)


    return X_train



def loss_table(train_history,test_history, save_folder, epoch = 0, validation_metric = 0,  save=False, timeforepoch=0):        #print the loss table during training
    print('{0:<22s} | {1:4s} | {2:15s} | {3:5s}| {4:5s}'.format(
            'component', "total_loss", "fake/true_loss", "AUX_loss", "ECAL_loss"))
    print('-' * 65)

    ROW_FMT = '{0:<22s} | {1:<4.2f} | {2:<15.2f} | {3:<5.2f}| {4:<5.2f}'
    print(ROW_FMT.format('generator (train)',
                         *train_history['generator'][-1]))
    print(ROW_FMT.format('generator (test)',
                         *test_history['generator'][-1]))
    print(ROW_FMT.format('discriminator (train)',
                         *train_history['discriminator'][-1]))
    print(ROW_FMT.format('discriminator (test)',
                         *test_history['discriminator'][-1]))
    if save == True: 
        if epoch == 0:
            f= open(save_folder + "/loss_table.txt","w")
        else:
            f= open(save_folder + "/loss_table.txt","a")
        str_epoch = "Epoch: " + str(epoch)
        f.write(str_epoch) 
        f.write("\n")
        f.write('{0:<22s} | {1:4s} | {2:15s} | {3:5s}| {4:5s}'.format('component', "total_loss", "fake/true_loss", "AUX_loss", "ECAL_loss"))
        f.write("\n")
        f.write('-' * 65) 
        f.write("\n")
        f.write(ROW_FMT.format('generator (train)', *train_history['generator'][-1])) 
        f.write("\n")
        f.write(ROW_FMT.format('generator (test)', *test_history['generator'][-1]))         
        f.write("\n")
        f.write(ROW_FMT.format('discriminator (train)', *train_history['discriminator'][-1])) 
        f.write("\n")
        f.write(ROW_FMT.format('discriminator (test)', *test_history['discriminator'][-1])) 
        e = timeforepoch
        f.write('\nTime for Epoch: {:02d}:{:02d}:{:02d}'.format(e // 3600, (e % 3600 // 60), e % 60))
        f.write("\nValidarion Metric: " + str(validation_metric))
        f.write("\nGromov Wasserstein Distance: " + str(train_history['Gromov_Wasserstein_validation'][-1]))
        f.write("\n\n")
        f.close()                    
    return

     
def plot_images(image_tensor, epoch, save_folder, save=False, number=1):    #plot images of trainingsdata or generator
    xx = np.linspace(1,25,25)
    yy = np.linspace(1,25,25)
    XX, YY = np.meshgrid(xx, yy)
    
    
    for i in range(number):#len(image_tensor)):
        dat=image_tensor[i]
        #print(dat.shape)
        ZZ =dat[:][:][13]
        #print(ZZ.shape)

        fig = plt.figure()
        ax = plt.axes(projection='3d')
        ax.plot_surface(YY, XX, ZZ, rstride=1, cstride=1, cmap='viridis', edgecolor='none')
        #ax.plot_wireframe(xx, yy, ZZ)
        ax.set_xlabel('x')
        ax.set_ylabel('z')
        ax.set_zlabel('Energy');
        ax.set_ylim(25, 0)    #invert y axes, that the particle enters form the front side
        number_epoch = str(epoch)
        number_epoch = number_epoch.zfill(4)
        plt.title("Epoch "+number_epoch)
        if save==True:
            plt.savefig(save_folder+"/Save_Images/plot_" + number_epoch + ".png")
        plt.show()         

